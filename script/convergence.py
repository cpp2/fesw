
import os
import matplotlib.pyplot as plt

""" 
@package docstring Berechnet fuer ein definiertes Dictionary die Fehler und erstellt eine csv mit den Konvergenzdaten.

Ausfuehren im Ordner CODE mit "python ./script/convergence.py"

"""
def main():
    
    n = [2**i for i in range(2, 5)]
    d = 2
    o = 1
    
    compDict = createDict(d, o, n)
    
    print(compDict)
    
    filename_fe = "bin/_release_sequential_O3/fe_solver"
    
    executeFEsolver(compDict, filename_fe)
    
    filenameNormConvergenceOutput = "output/conv.csv"
    
    plot(compDict)
    
    
    with open (filenameNormConvergenceOutput, 'w') as f:
        
        header = " n ,\t maxNorm ,\t H1-Seminorm ,\t ref h^1 ,\t ref h^2 ,\t ref h^3\n"

        f.write(header)
        
        for n, max, h1 in zip(compDict["n"], compDict["maxNorm"], compDict["H1-Seminorm"]):
            
            line = str(n) + " , \t" +  str(max) + " , \t" + str(h1) + " , \t" + str(1.0/(n)) + " , \t" + str(1.0/(n**2)) + " , \t" + str(1.0/(n**3)) + "\n"
#         
            f.write(line)



def plot(compDict):
    
    print(compDict["n"])
    print(compDict["maxNorm"])
    plt.loglog(compDict["n"], compDict["maxNorm"])
    plt.savefig("conv.png")
    plt.show()
    


def createDict(d,o,n):
    compDict = dict()
    
    compDict["dimension"] = d
    compDict["order"] = o
    compDict["n"] = n
    
    compDict["maxNorm"] = []
    compDict["H1-Seminorm"] = []
    
    return compDict
    
    

def readError(fname):
     
    with open(fname) as f:
        lines = [line.rstrip('\n') for line in f]
    
    norms = [float(x) for x in lines]
    
    return norms


def executeFEsolver(compDict, filename_fe):
    
    dim = compDict["dimension"]
    order = compDict["order"]
    
    for nVal in compDict["n"]:
        
        execLine = filename_fe + " -n " + str(nVal) + " -f convergence" + " -o " + str(order) + " -d " + str(dim) + " -s 0"
        print(execLine)
        
        os.system(execLine)
        
        norms = readError('output/convergence_err.txt')
        
        compDict["maxNorm"].append(norms[0])
        compDict["H1-Seminorm"].append(norms[1])
        
    print(compDict)

    return compDict


if __name__ == "__main__":
    main()






