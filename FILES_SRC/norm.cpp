
#include "norm.hpp"


std::vector<double> computeError(std::vector<node> &x, double (*analyticalSolution)(point *p), std::vector<double> &u)
{
    std::vector<double> error(x.size());
    LOOP(i, x.size()){

        error[i] = abs(u[i] - analyticalSolution(&x[i]));
    }
    return error;
}

double max_norm(std::vector<double> &u)
{
    double temp;
    double norm = 0.0;
    for(int i = 0; i < u.size(); i++)
    {
        temp = abs(u[i]);
        if (temp > norm)
            norm = temp;
    }
    return norm;
}

double h1_norm(std::vector<double> &u, sparseMatrix &M, sparseMatrix &K)
{
    double h1_s = h1_semi(u,K);
    double l2_n = l2_norm(u,M);
    return sqrt(pow(h1_s, 2) + pow(l2_n, 2));
}

double h1_semi(std::vector<double> &u, sparseMatrix &K)
{
    double norm = 0.0;
    std::vector<double> temp;
    temp.resize(u.size(),0.0);

    // Zuerst Matrix mal Spaltenvektor
    temp = K * u;

    // Jetzt Zeilenvektor mal Spaltenvektor
    for (int i = 0; i < u.size(); i++){
        norm += u[i] * (temp[i]);
    }

    return sqrt(norm);
}

double l2_norm(std::vector<double> &u, sparseMatrix &M){

    double norm = 0.0;
    std::vector<double> temp;
    temp.resize(u.size(),0.0);

    // Zuerst Matrix mal Spaltenvektor
    temp = M * u;

    // Jetzt Zeilenvektor mal Spaltenvektor
    for (int i = 0; i < u.size(); i++){
        norm += u[i] * temp[i];
    }

    return sqrt(norm);
}



