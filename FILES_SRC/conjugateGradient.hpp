/*!
 * \file conjugateGradient
 * \brief Konjugierte Gradienten Methode
 */

#pragma once


#include <stdio.h>
#include <vector>
#include <dirent.h>
#include <sys/stat.h>
#include <fstream>
#include <omp.h>
#include <assert.h>
#include <math_macros.hpp>

#include "math_toolbox.hpp"
#include "sparseMatrix.hpp"
#include "umfpack_solver.hpp"



/*! \brief CG-Verfahren zur Lösung linearer Gleichungssysteme
 * @param[in] verbose flag
 * @param[in] Systemmatrix A
 * @param[in] RHS b
 * @param[in] Loesungsvektor x
 */
void conjugateGradient(int verbose, sparseMatrix &A, std::vector <double> &b, std::vector <double> &x);

/*! \brief Vorkonditioniertes CG-Verfahren
 * @param[in] verbose flag
 * @param[in] Systemmatrix A
 * @param[in] RHS b
 * @param[in] Loesungsvektor x
 * @param[in] Vorkonditionierungstyp PrecType
 * @param[in] Blockgroesse blockSize (falls Block-Jacobi)
 */
void PrecConjugateGradient(int verbose, sparseMatrix &A, std::vector<double> &b, std::vector<double> &x, std::string PrecType, unsigned blockSize = 0, double tolerance = EPS);

/*!
 * \brief L2-Norm-Rechner
 * @param[in] Vektor x
 * @param[out] ||x||_{L2}
 */
double l2_norm(const std::vector<double>& x);

