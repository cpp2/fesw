#pragma once


#include <math.h>
#include <cstdlib>
#include <vector>
#include <iostream>

/*
 *@param[in] Vektor mit Funktionswerten der zu integrierenden Funktion an den entsprechenden Punkten
 *@param[in] Gewichte der Integrationsformel
 *@return Integral ueber das Referenzelement
*/
double integral_ref(const std::vector<double> &values, const std::vector<double> &weights) ;



