#pragma once


#include <meshClass.hpp>

#include "lgs.hpp"
#include "suitesparse/umfpack.h"

/*!
 * \brief Enthält die Lösungsroutinen, die auf UMFPACK basieren
 */



///Löse ein LGS mit UMFPACK
int solve_umfpack(int rows, int cols, sparseMatrix *K, std::vector<double> *b, std::vector<double> *u);
///Wende UMFPACK auf FE Problem an
int solve_FEM_umfpack(LGS *lgs, meshClass *mesh);



