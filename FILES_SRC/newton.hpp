#pragma once


#include <math_macros.hpp>
#include <vector>
#include <iostream>

#include "spMatrix.hpp"
#include "sparseMatrix.hpp"
#include "math_toolbox.hpp"

#include "conjugateGradient.hpp"
#include "meshClass.hpp"



/*!
 \brief Klasse für die Durchführung eines Newtonschrittes

 \version 1.0
 \author Christian Hotopp
 \date 2018

 */
class newton{

private:

    sparseMatrix *Jacobian ; ///< sparseMatrix für die Jacobische
    std::vector<double > *Fc_Vector ; ///< Zur Eingabe eines F, ausgewertet an der vorherigen Iteration
    std::vector<double > *cVector_tmp ; ///< Bei der Berechnung von c_{k+1} benötigen wir c_{k}
    std::vector<double > delta ; ///< Wir lösen nach c_{k-1}-c_{k}, müssen dies also auch speichern

    bool precon ; ///< Schalter für Newton mit/ohne Vorkonditionierer
    std::string PrecType ; ///< String für den zu verwendenden Vorkonditionierer
    unsigned blockSize ; ////< Größe der Blöcke, die beim BlockJacobi Vorkonditionierer verwendet werden sollen

public:

    ///Konstruktor
    newton(std::vector<double > *StartVector) ;
    ///Wenn mit Vorkonditionierer gerechnet werden soll, hier angeben!
    void setPrecon(std::string PrecType_in, unsigned blockSize_in) ;
    ///Setter für die Jacobische
    void setJac(sparseMatrix *Jac_in) ;
    ///Setter für die Auswertung von F in c
    void setF(std::vector<double > *F_in) ;
    ///Führe einen NewtonSchritt durch und überschreibe den Startwert mit dem Endergebnis
    /*! \brief Ein Newtonschritt
     * @param[in] verbose
     * @param[in] Referenz auf die Lösung
     */
    void newtonIterate(int verbose, std::vector<double > &c_out) ;

};


