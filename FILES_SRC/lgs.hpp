#pragma once


#include <iostream>
#include <vector>

#include "point.hpp"
#include "sparseMatrix.hpp"



/*!
 \brief Enthält Informationen über die für die Berechnung benötigten Parammeter

 \version 3.0
 \author Lukas Duechting
 \date 2018

 */
struct eqInf
{
public:
    ///Datenstruktur
    bool M;    ///< sparseMatrix für Massenmatrix
    bool K;    ///< sparseMatrix für Steifigkeitsmatrix
    bool u;    ///<  vector<double > für Lösungsvektor
    bool ut;    ///< vector<double > für Ableitung in der Zeit (wird für zeitabhängige Probleme benötigt)
    bool b;    ///< vector<double > für RHS des LGS

    eqInf()
    : M(false), K(false), u(false), ut(false), b(false)
    {
    }
    eqInf(bool m, bool k, bool u, bool ut, bool b)
    : M(m), K(k), u(u), ut(ut), b(b)
    {

    }
};


/*!
class for linear equation systems.

containing:
    sparseMatrix M;    sparseMatrix for mass matrix
    sparseMatrix K;    sparseMatrix for stiffness matrix
    vector<double> u;  double vector for storing the solution
    vector<double> ut; double vector for storing derivative of solution
    vector<double> b;  double vector for rhs

\author Lukas Duechting
\date 2018
\version 2.0
 \brief class for linear equation systems
 */
struct LGS
{
private:

protected:

public:
    /// information about required data structures
    eqInf reqData;

    ///data structures
    sparseMatrix M;    ///< sparseMatrix for mass matrix
    sparseMatrix K;    ///< sparseMatrix for stiffness matrix
    std::vector<double> u;    ///<  double vector for storing the solution
    std::vector<double> ut;    ///< double vector for storing derivative of solution
    std::vector<double> b;    ///< double vector for rhs

    double (*RHS_f)(point *p);
    double (*BC_dirichlet)(point *p);
    double (*BC_neumann)(point *p);

    /// trivial constructor
    LGS();

    /// constructor, setting up the information about required data structures
    LGS(bool m, bool k, bool u, bool ut, bool b);
};

