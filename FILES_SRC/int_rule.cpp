#include "int_rule.hpp"

double integral_ref(const std::vector<double> &values, const std::vector<double> &weights){
	// initialisiere das Integral als 0
	double integral = 0.0 ;

	// Summiere die Werte gewichtet auf
	for(int i=0; i< weights.size(); i++){
		integral += weights[i]*values[i] ;
	}
	// Gib das Integral zurueck
	return integral ;
}
