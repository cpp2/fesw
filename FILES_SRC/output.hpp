/*
 * output.hpp
 *
 *  Created on: 12.06.2018
 *      Author: mop
 */
#pragma once

#include <stdio.h>
#include <math.h>
#include <iomanip> // setprecision
#include <sstream> // stringstream
#include <iostream>// basic file operations
#include <fstream>// basic file operations

#include "myLib_wrapper.hpp"
#include <meshClass.hpp>

#include "lgs.hpp"


/*!
Functions for Postprocessing in Paraview
\author Christian Hotopp
\date 2018
\version 1.0
@param[in] filename base
@param[in] mesh pointer to object of finite element mesh
@param[in] LGS object pointer with solution
 */
///Exportieren der Lösung in ein für ParaView verständliches Format
void outputToParaview(std::string filename, meshClass &mesh, LGS &lgs);
///Abspeichern des Fehlers, wird vor allem für das Konvergenzskript script/convergence.py benötigt
void outputError(std::string filename, std::vector<double> error);
