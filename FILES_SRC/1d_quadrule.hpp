
#pragma once

# include <cstdlib>
# include <iostream>
# include <iomanip>
# include <cmath>
# include <ctime>
# include <cstring>

#include "myLib_wrapper.hpp"
#include "myLogger.hpp"


void imtqlx ( int n, double d[], double e[], double z[] );
double r8_abs ( double x );
double r8_epsilon ( );
double r8_gamma ( double x );
double r8_sign ( double x );
void r8mat_print ( int m, int n, double a[], std::string title );
void r8mat_print_some ( int m, int n, double a[], int ilo, int jlo, int ihi, int jhi, std::string title );
double *r8vec_even_new ( int n, double alo, double ahi );
void r8vec_print ( int n, double a[], std::string title );
void sgqf ( int nt, double aj[], double bj[], double zemu, double t[], double wts[] );
void timestamp_1d_quad ( );

