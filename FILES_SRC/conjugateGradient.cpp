
#include "conjugateGradient.hpp"


void conjugateGradient(int verbose, sparseMatrix &A, std::vector <double> &b, std::vector <double> &x){

    int n = b.size();

    //    cout << "Dimension: \t" << n << endl;

    // Startvektor x0 (Einsvektor)
    x.assign(n,1.0);

    if(verbose){
        std::cout << "Startvektor: \t";
        PRINT_VECTOR(x);
        std::cout << std::endl;
    }

    std::vector <double> temp(n,0.0);

    std::vector <double> r;
    std::vector<double> r_neu;
    std::vector <double> d;
    std::vector <double> z;

    matrixVectorMult(A,x,temp,n);

    r = b - temp;
    d = r;

    double alpha, beta;

    int k = 0;

    if (verbose){
        std::cout << "u_" << k << ": \t";
        PRINT_VECTOR(x);
        std::cout << std::endl << std::endl;
        std::cout << "||r_" << k << "||: \t" << l2_norm(r) << std::endl << std::endl;
    }

    while(l2_norm(r) > EPS){

        z.resize(n);

        // Matrix-Vektorprodukt einmal bestimmen, da es oefter gebraucht wird
        matrixVectorMult(A,d,z,n);

        alpha = innerProduct(r,r) / innerProduct(d,z);

        x = x + (alpha*d);

        r_neu = r - (alpha*z);

        beta = innerProduct(r_neu,r_neu) / innerProduct(r,r);

        d = r_neu + (beta*d);

        r = r_neu;

        if(verbose){
            std::cout << "u_" << k+1 << ": \t";
            PRINT_VECTOR(x);
            std::cout << std::endl << std::endl;

            std::cout << "||r_" << k+1 << "||: \t" << l2_norm(r) << std::endl << std::endl;
        }

        k++;

    }

    std::cout << std::endl <<"CG-METHOD CONVERGED AFTER " << k << " ITERATIONS." << std::endl;

}

void PrecConjugateGradient(int verbose, sparseMatrix &A, std::vector<double> &b, std::vector<double> &x, std::string PrecType, unsigned blockSize, double tolerance){

    assert(blockSize <= A.getRows());

    int n = b.size();
    sparseMatrix C;
    sparseMatrix M;
    // Startvektor x0 (Einsvektor)
    x.assign(n,1.0);
    // Residuumsvektor
    std::vector <double> r;
    // h = C * r
    std::vector<double> h(n,0.0);
    std::vector <double> temp(n,0.0);

    // Bestimme Residuum des Startvektors
    matrixVectorMult(A,x,temp,n);

    r = b - temp;

//    cout << endl << "r0 = " << endl;
//    PRINT_VECTOR(r);
//    cout << endl;

    if (PrecType == "jacobi" || PrecType == "Jacobi"){

        // Erstelle Vorkonditionierer-Matrix C ( = D^(-1) , wobei D = diag(A) )
        C = A.diag_inv();

        matrixVectorMult(C,r,temp,n);
        h = temp;

//        cout << endl << "h0 = " << endl;
//        PRINT_VECTOR(h);
//        cout << endl;

    }

    else if (PrecType == "blockjacobi" || PrecType == "BlockJacobi"){

        // Erstelle die Blockdiagonalmatrix
        std::vector<sparseMatrix> blocklist = A.blockdiag(blockSize);

        if (blockSize <= 3){

            // Erstelle Block-Jacobi-Vorkonditionierer-Matrix (UMWEG UEBER DENSEMATRIX2)
            // FUNKTIONIERT ERSTMAL NUR FUER BLOCKSIZE <= 3


            std::vector<denseMatrix2> blocklistDense(blocklist.size());
            denseMatrix2 tempM;

            // Wandle die sparse-Matrizen ins denseMatrix2-Format um und invertiere sie.
            LOOP(block,blocklist.size()){
                tempM.fillFromSparse(blocklist[block]);
                //            cout << "Groesse der Blockmatrix: \t" << temp.getRows() <<endl;
                blocklistDense[block] = tempM.getInverse();
            }

            // Transformiere ins sparse-Format zurueck
            sparseMatrix tempM2;
            LOOP(block,blocklistDense.size()){
                tempM2.fillFromDense(&blocklistDense[block]);
                blocklist[block] = tempM2;
            }

            // Erstelle eine grosse sparseMatrix, die dann unser C ist.
            C.appendBlocks(blocklist);

            //        cout << endl << "MATRIX C: " << endl;
            //        C.print();

            matrixVectorMult(C,r,temp,n);
            h = temp;

        }

        else {

            M.appendBlocks(blocklist);

            solve_umfpack(M.getRows(),M.getColumns(),&M,&r,&h);

//            cout << endl << "h0 = " << endl;
//            PRINT_VECTOR(h);
//            cout << endl;

        }
    }

    else {
        std::cout << "Dieser Vorkonditionierungstyp ist nicht verfuegbar." << std::endl;
    }


    if(verbose)
    {
        std::cout << std::endl << "Startvektor: \t";
        PRINT_VECTOR(x);
        std::cout << std::endl;
    }

    std::vector<double> r_neu;
    std::vector<double> h_neu(n,0.0);
    std::vector <double> d;
    std::vector <double> z;

    d = h;

    double alpha, beta;

    int k = 0;

    if(verbose)
    {
        std::cout << "u_" << k << ": \t";
        PRINT_VECTOR(x);
        std::cout << std::endl << std::endl;
        std::cout << "||r_" << k << "||: \t" << l2_norm(r) << std::endl << std::endl;
    }

    while (l2_norm(r) > tolerance){
        z.resize(n);
        // Matrix-Vektorprodukt einmal bestimmen, da es oefter gebraucht wird
        matrixVectorMult(A,d,z,n);

        alpha = innerProduct(r,h) / innerProduct(d,z);

        x = x + (alpha * d);

        r_neu = r - (alpha * z);

        if (blockSize > 3){
            solve_umfpack(M.getRows(),M.getColumns(),&M,&r_neu,&h_neu);
        }

        else {
            matrixVectorMult(C,r_neu,temp,n);
            h_neu = temp;
        }

        beta = innerProduct(r_neu,h_neu) / innerProduct(r,h) ;

        d = h_neu + (beta * d);

        r = r_neu;
        h = h_neu;

        if(verbose)
        {
            std::cout << "u_" << k+1 << ": \t";
            PRINT_VECTOR(x);
            std::cout << std::endl << std::endl;

            std::cout << "||r_" << k+1 << "||: \t" << l2_norm(r) << std::endl << std::endl;
        }

        k++;
    }

    std::cout << std::endl << PrecType << "-PCG-METHOD CONVERGED AFTER " << k << " ITERATIONS." << std::endl;
}

double l2_norm(const std::vector<double>& x)
{

    return sqrt(innerProduct(x,x));

}
