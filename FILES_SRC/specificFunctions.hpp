#pragma once

#include <stdio.h>
#include <math.h>
#include <iostream>

#include "math_macros.hpp"
#include "point.hpp"



/*! \file specificFunctions.hpp
 * Hier werden alle fuer uns relevanten Funktionen (RHS, Dirichlet-Funktionen, exakte Loesungen) gesammelt. Alle Funktionen
 * bekommen einen Knoten und werten die jeweilige Funktion an diesem aus.
 */

/*!
 * Homogene Dirichlet-Randbedingung.
 */
double dir0(point *p);

/*!
 * Inhomogene Dirichlet-Randbedingung.
 */
double dir1(point *p);



/*!
 * Ueberprueft, ob Punkt auf Neumann-Rand liegt.
 */
bool isNeumann(point *p);


/*!
 * Rechte Seite-Funktion (Vgl. Programmieraufgabe 1, Uebungsblatt 7, NumPDGL, Klawonn).
 */
double rhs_function_testgrid_sin(point *x);

/*!
 * Inhomogene Rechte Seite (Vgl. Programmieraufgabe 2, Uebungsblatt 7, NumPDGL, Klawonn).
 */
double rhs_f1(point *x);

/// exakte Lösung für rhs_f1
double u_exact_testgrid_1(point *x);

/// exakte Lösung für rhs_function_testgrid_sin
double u_exact_testgrid_sin(point *x);



