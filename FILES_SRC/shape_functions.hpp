#pragma once

#include <math.h>
#include <cstdlib>
#include <vector>
#include <iostream>

#include "point.hpp"
#include "denseMatrix.hpp"
#include "meshClass.hpp"
#include "umfpack_solver.hpp"


/*!
 *\brief Klasse für die Formfunktionen
 *
 *\version 2.0
 *\author Christian Hotopp
 *\date 2018
 */
class shape_functions{
private:
    std::vector<std::vector<double> > Phi_coeff ; ///< Koeffizientenvektoren der Basisfunktionen
    std::vector<std::vector<std::vector<double> > > GradPhi_coeff ; ///< Koeffizientenfunktionen der Gradienten der Basisfunktionen
    int order ; ///< Ordnung der FE Ansatzfunktionen
    int nPhi ; ///< Anzahl (Länge der Koeffizientenvektoren) der Basisfunktionen
public:
    ///leerer Konstruktor
    shape_functions();
    ///Konstruktor für ein Basisfunktionsobjekt
    shape_functions(int verbose, int order_in);
    ///Setze Koeffizienten der Basisfunktionen fuer gegebene Ordnung (2D)
    void load_shape_functions_2D(int verbose, int order_in) ;
    ///Berechne die Basisfunktionen auf Basis eines gegebenen Referenzelementes (3D)
    void compute_shape_functions(int verbose, meshClass &refMesh ) ;
    ///Lade Koeffizienten der Basisfunktionen in 1D
    void load_shape_functions_1D(int verbose, int order_in);
    ///Getter für den Wert der i-ten Basisfunktion in p
    double getPhi(const point *p, const int i);
    ///Auswertung des -iten Gradienten im Punkt pin, schreibe in pout
    void getGradPhiI(const point *pin, const int i, point *pout);
    ///Getter fuer die Ordnung
    int getOrder() ;
    ///Getter fuer Anzahl an Basisfunktionen
    int getnPhi();

    /* Funktion um Polynome mit gegebenen Koeffizienten auszuwerten */
    /*
     *@param[in] Vektor mit Koeffizienten s.h. unten
     *@param[in] point *p, Zeiger auf den Punkt, an dem die jeweilige Basisfunktion ausgewertet werden soll
     *@param[out] Wert der durch die Koeffizienten bestimmten Funktion am Punkt *p
     */
    double polyval(std::vector<double> *coeff, const point *p) ;

} ;



