
#include "newton.hpp"



newton::newton(std::vector<double > *StartVector){
    // Hier steht später das Zwischenergebnis
    cVector_tmp = StartVector ;

    Jacobian  = NULL ;
    Fc_Vector = NULL ;

    delta.resize(cVector_tmp->size(), 0.0) ;
    precon = false ;
    PrecType = "" ;
    blockSize = 0 ;
}

void newton::setPrecon(std::string PrecType_in, unsigned blockSize_in){
    PrecType = PrecType_in ;
    precon   = true ;
    blockSize = blockSize_in ;
}

void newton::setJac(sparseMatrix *Jac_in){
    Jacobian = Jac_in ;
}

void newton::setF(std::vector<double > *F_in){
    Fc_Vector = F_in ;
}

void newton::newtonIterate(int verbose, std::vector<double > &c_out){

    if (verbose == 1){
        std::cout << std::endl << "\t \t Beginne Newtonschritt" << std::endl ;
        std::cout << "F(c) = " ;

        PRINT_VECTOR(*Fc_Vector) ;

        std::cout << std::endl << "c_tmp = " ;

        PRINT_VECTOR(*cVector_tmp) ;

        std::cout << std::endl << std::endl ;
    }

    // Loese J(c)*delta = - F(c)
//    *Fc_Vector = ( -1.0) * (*Fc_Vector) ;

    if (verbose == 1){
        std::cout << std::endl << "-F(c) = " ;
        PRINT_VECTOR(*Fc_Vector) ;
        std::cout << std::endl ;
    }

    if(precon == false){
        conjugateGradient(verbose, *Jacobian, *Fc_Vector, delta) ;
    }

    else if(precon == true){
        PrecConjugateGradient(verbose, *Jacobian, *Fc_Vector, delta, PrecType, blockSize) ;
    }

    if (verbose == 1){
        std::cout << std::endl << "delta = " ;
        PRINT_VECTOR(delta) ;
        std::cout << std::endl << "c_out = " ;
        PRINT_VECTOR(c_out) ;
        std::cout << std::endl << std::endl ;
    }

    // Berechne c_{k+1} = delta - c_{k}
    c_out = delta - *cVector_tmp ;

    c_out = (-1.0) * c_out ;

    if (verbose == 1){
        std::cout << std::endl << "Nach Berechnung: c_out = " ;
        PRINT_VECTOR(c_out) ;
        std::cout << std::endl << std::endl ;
    }

    // Setze c_{k} = c_{k+1}
    cVector_tmp = &c_out ;
}
