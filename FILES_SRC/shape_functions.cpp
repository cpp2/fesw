#include "shape_functions.hpp"

shape_functions::shape_functions():order(0),nPhi(0) { } ;

shape_functions::shape_functions(int verbose, int order_in) {
    load_shape_functions_2D(verbose, order_in) ;
}

void shape_functions::load_shape_functions_1D(int verbose, int order_in)
{
    // Setze die Ordnung auf die eingegebene
    order = order_in ;

    // Basisfunktionen fuer Ordnung 1
    if (order == 1)
    {
        nPhi = 2 ;
        // allokiere Speicher fuer die Vektoren mit den Koeffizientenvektoren
        Phi_coeff.resize(nPhi) ;
        GradPhi_coeff.resize(nPhi) ;

        // setze die Laenge der Koeffizientenvektoren und initialisiere mit 0
        for(int i=0; i< nPhi ; i++)
        {
            Phi_coeff[i].resize(nPhi, 0.0) ;
            GradPhi_coeff[i].resize(1) ;
            for (int k=0; k<2; k++){
                GradPhi_coeff[i][k].resize(nPhi, 0.0) ;
            }
        }

        if(verbose)
            std::cout << "Basisfunktionen fuer Ordnung 1 werden bestimmt." << std::endl ;

        // Phi_1 = 1 - x
        Phi_coeff[0][0] = 1 ;
        Phi_coeff[0][1] = -1 ;
        // Phi_2 = x
        Phi_coeff[1][1] = 1 ;

        // GradPhi_1_x = -1
        GradPhi_coeff[0][0][0] = -1 ;
        // GradPhi_2_x = 1
        GradPhi_coeff[1][0][0] = 1 ;

    }
    else if (order == 2)
    {
        nPhi = 3 ;
        // allokiere Speicher fuer die Vektoren mit den Koeffizientenvektoren
        Phi_coeff.resize(nPhi) ;
        GradPhi_coeff.resize(nPhi) ;

        // setze die Laenge der Koeffizientenvektoren und initialisiere mit 0
        for(int i=0; i< nPhi ; i++)
        {
            Phi_coeff[i].resize(nPhi, 0.0) ;
            GradPhi_coeff[i].resize(1) ;
            for (int k=0; k<2; k++){
                GradPhi_coeff[i][k].resize(nPhi-1, 0.0) ;
            }
        }

        if(verbose)
            std::cout << "Basisfunktionen fuer Ordnung 1 werden bestimmt." << std::endl ;

        // Phi_1 = 1 - 3x + 2x^2
        Phi_coeff[0][0] = 1 ;
        Phi_coeff[0][1] = -3 ;
        Phi_coeff[0][2] = 2 ;
        // Phi_2 = -x + 2x^2
        Phi_coeff[1][1] = -1 ;
        Phi_coeff[1][2] = 2 ;
        // Phi_3 = 4x - 4 x^2
        Phi_coeff[2][1] = 4 ;
        Phi_coeff[2][2] = -4 ;

        // GradPhi_1_x = -3 + 4x
        GradPhi_coeff[0][0][0] = -3 ;
        GradPhi_coeff[0][0][1] = 4 ;
        // GradPhi_2_x = -1 + 4x
        GradPhi_coeff[1][0][0] = -1 ;
        GradPhi_coeff[1][0][1] = 4 ;
        // GradPhi_3_x = 4 - 8x
        GradPhi_coeff[2][0][0] = 4 ;
        GradPhi_coeff[2][0][1] = -8 ;

    }

    assert(order == 1 || order == 2);

    // Fehlermeldung, falls ungueltige Ordnung eingegeben wurde
    //    else {
    //        cerr << "exception: " << out_of_range("Die eingegebene Ordnung ist leider nicht verfuegbar.").what() << endl;
    //        exit(EXIT_FAILURE);
    //    }
}

void shape_functions::load_shape_functions_2D(int verbose, int order_in){
    // Setze die Ordnung auf die eingegebene
    order = order_in ;

    // Basisfunktionen fuer Ordnung 1
    if (order == 1) {
        nPhi = 3 ;
        // allokiere Speicher fuer die Vektoren mit den Koeffizientenvektoren
        Phi_coeff.resize(nPhi) ;
        GradPhi_coeff.resize(nPhi) ;

        // setze die Laenge der Koeffizientenvektoren und initialisiere mit 0
        for(int i=0; i< Phi_coeff.size() ; i++){
            Phi_coeff[i].resize(nPhi, 0.0) ;
            GradPhi_coeff[i].resize(2) ;
            for (int k=0; k<2; k++){
                GradPhi_coeff[i][k].resize(nPhi, 0.0) ;
            }
        }

        if(verbose)
            std::cout << "Basisfunktionen fuer Ordnung 1 werden bestimmt." << std::endl ;

        // Phi_1 = 1 - x - y
        Phi_coeff[0][0] = 1 ;
        Phi_coeff[0][1] = -1 ;
        Phi_coeff[0][2] = -1 ;
        // Phi_2 = x
        Phi_coeff[1][1] = 1 ;
        // Phi_3 = y
        Phi_coeff[2][2] = 1 ;

        // GradPhi_1_x = -1
        GradPhi_coeff[0][0][0] = -1 ;
        // GradPhi_1_y = -1
        GradPhi_coeff[0][1][0] = -1 ;
        // GradPhi_2_x = 1
        GradPhi_coeff[1][0][0] = 1 ;
        // GradPhi_2_y = 0
        //
        // GradPhi_3_x = 0
        //
        // GradPhi_3_y = 1
        GradPhi_coeff[2][1][0] = 1 ;
    }
    // Basisfunktionen fuer Ordnung 2
    else if (order == 2) {
        nPhi=6;
        // allokiere Speicher fuer die Vektoren mit den Koeffizientenvektoren
        Phi_coeff.resize(nPhi) ;
        GradPhi_coeff.resize(nPhi) ;
        // setze die Laenge der Koeffizientenvektoren und initialisiere mit 0
        for(int i=0; i< Phi_coeff.size() ; i++){
            Phi_coeff[i].resize(nPhi, 0.0) ;
            GradPhi_coeff[i].resize(2) ;
            for (int k=0; k<2; k++){
                GradPhi_coeff[i][k].resize(nPhi, 0.0) ;
            }
        }

        if(verbose)
            std::cout << "Basisfunktionen fuer Ordnung 2 werden bestimmt." << std::endl ;

        // Phi_1 = 1 - 3x - 3y + 4xy + 2x^2 + 2y^2
        Phi_coeff[0][0] = 1 ;
        Phi_coeff[0][1] = -3 ;
        Phi_coeff[0][2] = -3 ;
        Phi_coeff[0][3] = 4 ;
        Phi_coeff[0][4] = 2 ;
        Phi_coeff[0][5] = 2 ;
        // Phi_2 = -x + 2*x^2
        Phi_coeff[1][1] = -1 ;
        Phi_coeff[1][4] = 2 ;
        // Phi_3 = -y + 2*y^2
        Phi_coeff[2][2] = -1 ;
        Phi_coeff[2][5] = 2 ;
        // Phi_4 = 4x - 4xy - 4x^2
        Phi_coeff[3][1] = 4 ;
        Phi_coeff[3][3] = -4 ;
        Phi_coeff[3][4] = -4 ;
        // Phi_5 = 4xy
        Phi_coeff[4][3] = 4 ;
        // Phi_6 = 4y - 4xy - 4y^2
        Phi_coeff[5][2] = 4 ;
        Phi_coeff[5][3] = -4 ;
        Phi_coeff[5][5] = -4 ;

        // GradPhi_1_x = -3 + 4x + 4y
        GradPhi_coeff[0][0][0] = -3 ;
        GradPhi_coeff[0][0][1] = 4 ;
        GradPhi_coeff[0][0][2] = 4 ;
        // GradPhi_1_y = -3 + 4x + 4y
        GradPhi_coeff[0][1][0] = -3 ;
        GradPhi_coeff[0][1][1] = 4 ;
        GradPhi_coeff[0][1][2] = 4 ;
        // GradPhi_2_x = -1 + 4x
        GradPhi_coeff[1][0][0] = -1 ;
        GradPhi_coeff[1][0][1] = 4 ;
        // GradPhi_2_y = 0
        //
        // GradPhi_3_x = 0
        //
        // GradPhi_3_y = -1 + 4y
        GradPhi_coeff[2][1][0] = -1 ;
        GradPhi_coeff[2][1][2] = 4 ;
        // GradPhi_4_x = 4 - 8x - 4y
        GradPhi_coeff[3][0][0] = 4 ;
        GradPhi_coeff[3][0][1] = -8 ;
        GradPhi_coeff[3][0][2] = -4 ;
        // GradPhi_4_y = - 4x
        GradPhi_coeff[3][1][1] = -4 ;
        // GradPhi_5_x = 4y
        GradPhi_coeff[4][0][2] = 4 ;
        // GradPhi_5_y = 4x
        GradPhi_coeff[4][1][1] = 4 ;
        // GradPhi_6_x = - 4y
        GradPhi_coeff[5][0][2] = -4 ;
        // GradPhi_6_y = 4 - 4x - 8y
        GradPhi_coeff[5][1][0] = 4 ;
        GradPhi_coeff[5][1][1] = -4 ;
        GradPhi_coeff[5][1][2] = -8 ;

    }

    // Basisfunktionen fuer Ordnung 3
    else if (order == 3 ) {
        nPhi=10;
        // allokiere Speicher fuer die Vektoren mit den Koeffizientenvektoren
        Phi_coeff.resize(nPhi) ;
        GradPhi_coeff.resize(nPhi) ;
        // setze die Laenge der Koeffizientenvektoren und initialisiere mit 0
        for(int i=0; i< Phi_coeff.size() ; i++){
            Phi_coeff[i].resize(nPhi, 0.0) ;
            GradPhi_coeff[i].resize(2) ;
            for (int k=0; k<2; k++){
                GradPhi_coeff[i][k].resize(nPhi, 0.0) ;
            }
        }

        if(verbose)
            std::cout << "Basisfunktionen fuer Ordnung 3 werden bestimmt." << std::endl ;

        // Phi_1 = 1 - 5.5x - 5.5y + 18xy + 9x^2 + 9y^2 - 13.5 x^2y - 13.5xy^2 - 4.5x^3 - 4.5y^3
        Phi_coeff[0][0] = 1 ;
        Phi_coeff[0][1] = -5.5 ;
        Phi_coeff[0][2] = -5.5 ;
        Phi_coeff[0][3] = 18 ;
        Phi_coeff[0][4] = 9 ;
        Phi_coeff[0][5] = 9 ;
        Phi_coeff[0][6] = -13.5 ;
        Phi_coeff[0][7] = -13.5 ;
        Phi_coeff[0][8] = -4.5 ;
        Phi_coeff[0][9] = -4.5 ;
        // Phi_2 = x - 4.5x^2 + 4.5x^3
        Phi_coeff[1][1] = 1 ;
        Phi_coeff[1][4] = -4.5 ;
        Phi_coeff[1][8] = 4.5 ;
        // Phi_3 = y - 4.5y^2 + 4.5y^3
        Phi_coeff[2][2] = 1 ;
        Phi_coeff[2][5] = -4.5 ;
        Phi_coeff[2][9] = 4.5 ;
        // Phi_4 = 9x - 22.5xy - 22.5x^2 + 27x^2y + 13.5 xy^2 + 13.5x^3
        Phi_coeff[3][1] = 9 ;
        Phi_coeff[3][3] = -22.5 ;
        Phi_coeff[3][4] = -22.5 ;
        Phi_coeff[3][6] = 27 ;
        Phi_coeff[3][7] = 13.5 ;
        Phi_coeff[3][8] = 13.5 ;
        // Phi_5 = - 4.5x + 4.5xy + 18x^2 - 13.5x^2y - 13.5x^3
        Phi_coeff[4][1] = -4.5 ;
        Phi_coeff[4][3] = 4.5 ;
        Phi_coeff[4][4] = 18 ;
        Phi_coeff[4][6] = -13.5 ;
        Phi_coeff[4][8] = -13.5 ;
        // Phi_6 = - 4.5xy + 13.5x^2y
        Phi_coeff[5][3] = -4.5 ;
        Phi_coeff[5][6] = 13.5 ;
        // Phi_7 = -4.5xy + 13.5xy^2
        Phi_coeff[6][3] = -4.5 ;
        Phi_coeff[6][7] = 13.5 ;
        // Phi_8 = - 4.5y + 4.5xy + 18y^2 - 13.5xy^2 - 13.5y^3
        Phi_coeff[7][2] = -4.5 ;
        Phi_coeff[7][3] = 4.5 ;
        Phi_coeff[7][5] = 18 ;
        Phi_coeff[7][7] = -13.5 ;
        Phi_coeff[7][9] = -13.5 ;
        // Phi_9 = 9y - 22.5xy - 22.5y^2 + 13.5x^2y + 27xy^2 + 13.5y^3
        Phi_coeff[8][2] = 9 ;
        Phi_coeff[8][3] = -22.5 ;
        Phi_coeff[8][5] = -22.5 ;
        Phi_coeff[8][6] = 13.5 ;
        Phi_coeff[8][7] = 27 ;
        Phi_coeff[8][9] = 13.5 ;
        // Phi_10 = 27xy - 27x^2y - 27xy^2
        Phi_coeff[9][3] = 27 ;
        Phi_coeff[9][6] = -27 ;
        Phi_coeff[9][7] = -27 ;

        // GradPhi_1_x = - 5.5 + 18x + 18y - 27xy - 13.5x^2 - 13.5y^2
        GradPhi_coeff[0][0][0] = -5.5 ;
        GradPhi_coeff[0][0][1] = 18 ;
        GradPhi_coeff[0][0][2] = 18 ;
        GradPhi_coeff[0][0][3] = -27 ;
        GradPhi_coeff[0][0][4] = -13.5 ;
        GradPhi_coeff[0][0][5] = -13.5 ;
        // GradPhi_1_y = - 5.5 + 18x + 18y - 27xy - 13.5x^2 - 13.5y^2
        GradPhi_coeff[0][1][0] = -5.5 ;
        GradPhi_coeff[0][1][1] = 18 ;
        GradPhi_coeff[0][1][2] = 18 ;
        GradPhi_coeff[0][1][3] = -27 ;
        GradPhi_coeff[0][1][4] = -13.5 ;
        GradPhi_coeff[0][1][5] = -13.5 ;
        // GradPhi_2_x = 1 - 9x + 13x^2
        GradPhi_coeff[1][0][0] = 1 ;
        GradPhi_coeff[1][0][1] = -9 ;
        GradPhi_coeff[1][0][4] = 13 ;
        // GradPhi_2_y = 0
        //
        // GradPhi_3_x = 0
        //
        // GradPhi_3_y = 1 - 9y + 13y^2
        GradPhi_coeff[2][1][0] = 1 ;
        GradPhi_coeff[2][1][2] = -9 ;
        GradPhi_coeff[2][1][5] = 13 ;
        // GradPhi_4_x = 9 - 45x - 22.5y + 54xy + 40.5x^2 + 13.5y^2
        GradPhi_coeff[3][0][0] = 9 ;
        GradPhi_coeff[3][0][1] = -45 ;
        GradPhi_coeff[3][0][2] = -22.5 ;
        GradPhi_coeff[3][0][3] = 54 ;
        GradPhi_coeff[3][0][4] = 40.5 ;
        GradPhi_coeff[3][0][5] = 13.5 ;
        // GradPhi_4_y = - 22.5x + 27xy + 27x^2
        GradPhi_coeff[3][1][1] = -22.5 ;
        GradPhi_coeff[3][1][3] = 27 ;
        GradPhi_coeff[3][1][4] = 27 ;
        // GradPhi_5_x = - 4.5 + 36x + 4.5y - 27xy - 40.5x^2
        GradPhi_coeff[4][0][0] = 4.5 ;
        GradPhi_coeff[4][0][1] = 36 ;
        GradPhi_coeff[4][0][2] = 4.5 ;
        GradPhi_coeff[4][0][3] = -27 ;
        GradPhi_coeff[4][0][4] = -40.5 ;
        // GradPhi_5_y = 4.5x - 13.5x^2
        GradPhi_coeff[4][1][1] = 4.5 ;
        GradPhi_coeff[4][1][4] = -13.5 ;
        // GradPhi_6_x = - 4.5y + 27xy
        GradPhi_coeff[5][0][2] = -4.5 ;
        GradPhi_coeff[5][0][3] = 27 ;
        // GradPhi_6_y = - 4.5x + 13.5x^2
        GradPhi_coeff[5][1][1] = -4.5 ;
        GradPhi_coeff[5][1][4] = 13.5 ;
        // GradPhi_7_x = - 4.5y + 13.5y^2
        GradPhi_coeff[6][0][2] = -4.5 ;
        GradPhi_coeff[6][0][5] = 13.5 ;
        // GradPhi_7_y = - 4.5x + 27xy
        GradPhi_coeff[6][1][1] = -4.5 ;
        GradPhi_coeff[6][1][3] = 27 ;
        // GradPhi_8_x = 4.5y - 13.5y^2
        GradPhi_coeff[7][0][2] = 4.5 ;
        GradPhi_coeff[7][0][5] = -13.5 ;
        // GradPhi_8_y = - 4.5 + 4.5x + 36y - 27xy - 40.5y^2
        GradPhi_coeff[7][1][0] = -4.5 ;
        GradPhi_coeff[7][1][1] = 4.5 ;
        GradPhi_coeff[7][1][2] = 36 ;
        GradPhi_coeff[7][1][3] = -27 ;
        GradPhi_coeff[7][1][5] = -40.5 ;
        // GradPhi_9_x = - 22.5y + 27xy + 27y^2
        GradPhi_coeff[8][0][2] = -22.5 ;
        GradPhi_coeff[8][0][3] = 27 ;
        GradPhi_coeff[8][0][5] = 27 ;
        // GradPhi_9_y = 9 - 22.5x - 45y + 54xy + 13.5x^2 + 40.5y^2
        GradPhi_coeff[8][1][0] = 9 ;
        GradPhi_coeff[8][1][1] = -22.5 ;
        GradPhi_coeff[8][1][2] = -45 ;
        GradPhi_coeff[8][1][3] = 54 ;
        GradPhi_coeff[8][1][4] = 13.5 ;
        GradPhi_coeff[8][1][5] = 40.5 ;
        // GradPhi_10_x = 27y - 54xy - 27y^2
        GradPhi_coeff[9][0][2] = 27 ;
        GradPhi_coeff[9][0][3] = -54 ;
        GradPhi_coeff[9][0][5] = -27 ;
        // GradPhi_10_y = 27x - 54xy - 27x^2
        GradPhi_coeff[9][1][1] = 27 ;
        GradPhi_coeff[9][1][3] = -54 ;
        GradPhi_coeff[9][1][4] = -27 ;

    }

    // Fehlermeldung, falls ungueltige Ordnung eingegeben wurde
    else {
        std::out_of_range("Die eingegebene Ordnung ist leider nicht verfuegbar.") ;
    }
}

void shape_functions::compute_shape_functions(int verbose, meshClass &refMesh ) {
    std::vector<node > points  = *refMesh.getNodelist() ;
    int dimension         = refMesh.getnDimension() ;

    if (dimension == 1)
    {
        load_shape_functions_1D(verbose, refMesh.getnOrder());
    }
    if (dimension == 2)
    {
        load_shape_functions_2D(verbose, refMesh.getnOrder());
    }
    else if (dimension == 3)
    {
        double x, y, z ;
        this->nPhi = points.size() ;
        int nGradPhi ;
        //Gradientenkoeffizientenvektorlaengenbestimmungsabfragefunktionsblock
        if(nPhi == 4){nGradPhi = 1 ;}
        else if(nPhi == 10){nGradPhi = 4 ;}
        else if(nPhi == 20){nGradPhi = 10 ;}


        denseMatrix Vandermonde(nPhi, nPhi ) ;
        // Erstelle die Vandermonde Matrix zeilenweise
        LOOP(row, nPhi )
        {
            x = points[row].getXi(0) ; // hole die Koordinaten des Knotens
            y = points[row].getXi(1) ;
            z = points[row].getXi(2) ;
            // Auswertung der Monombasis fuer P1
            Vandermonde.setEntry(row, 0, 1.0) ; // 1
            Vandermonde.setEntry(row, 1, x) ; // x
            Vandermonde.setEntry(row, 2, y) ; // y
            Vandermonde.setEntry(row, 3, z) ; // z
            // ggf Hinzunahme der Monombasisauswertung fuer P2
            if(nPhi > 4){
                Vandermonde.setEntry(row, 4, x*x) ; // x*x
                Vandermonde.setEntry(row, 5, x*y) ; // x*y
                Vandermonde.setEntry(row, 6, y*y) ; // y*y
                Vandermonde.setEntry(row, 7, x*z) ; // x*z
                Vandermonde.setEntry(row, 8, y*z) ; // y*z
                Vandermonde.setEntry(row, 9, z*z) ; // z*z
            }
            // ggf Hinzunahme der Monombasisauswertung fuer P3
            if(nPhi > 10){
                Vandermonde.setEntry(row, 10, x*x*x) ; // x*x*x
                Vandermonde.setEntry(row, 11, x*x*y) ; // x*x*y
                Vandermonde.setEntry(row, 12, x*y*y) ; // x*y*y
                Vandermonde.setEntry(row, 13, y*y*y) ; // y*y*y
                Vandermonde.setEntry(row, 14, x*x*z) ; // x*x*z
                Vandermonde.setEntry(row, 15, x*y*z) ; // x*y*z
                Vandermonde.setEntry(row, 16, y*y*z) ; // y*y*z
                Vandermonde.setEntry(row, 17, x*z*z) ; // x*z*z
                Vandermonde.setEntry(row, 18, y*z*z) ; // y*z*z
                Vandermonde.setEntry(row, 19, z*z*z) ; // z*z*z
            }
        }
        // Allokiere den Speicher fuer die Koeffizientenvektoren
        Phi_coeff.resize(nPhi ) ;
        GradPhi_coeff.resize(nPhi ) ;

        LOOP(k, nPhi ){
            Phi_coeff[k].resize(nPhi, 0.0 ) ;
            // Jeder Gradient enthaelt 3 Koeffizientenvektoren
            GradPhi_coeff[k].resize(3) ;
            GradPhi_coeff[k][0].resize(nGradPhi, 0.0) ;
            GradPhi_coeff[k][1].resize(nGradPhi, 0.0) ;
            GradPhi_coeff[k][2].resize(nGradPhi, 0.0) ;
        }
        /* Die Koeffizienten ergeben sich jetzt als Loesung des Systems Va=1,
         * wobei V die Vandermondematrix, a der Koeffizientenvektor und 1 die Einheitsmatrix
         * entsprechender Groesse sein sollen. Die Loesung dieses Systems enthaelt spaltenweise die
         * Koeffizientenvektoren der Ansatzfunktionen. */
        LOOP(k, nPhi ){
            // UMFPACK braucht sparseMatrizen
            sparseMatrix *VandermondeSP = new sparseMatrix(nPhi, nPhi ) ;
            VandermondeSP->fillFromDense(&Vandermonde) ;
            // Erzeuge k-ten kanonischen Basisvektor fuer die Kronecker Eigenschaft der Phi_i
            std::vector<double > BasisVector ; // allokiere und fuelle mit 0
            BasisVector.resize(nPhi, 0.0) ;
            // Setze k-ten Eintrag auf 1
            BasisVector[k] = 1.0 ;

            // Loese das System mit UMFPACK
            solve_umfpack(nPhi, nPhi, VandermondeSP, &BasisVector, &Phi_coeff[k] ) ;
            if (verbose == 1){
                std::cout << std::endl << "Die Koeffizienten der " << k << "ten Basisfunktion sind:     " ;
                PRINT_VECTOR(Phi_coeff[k] ) ;
            }
            //Ableitung von Phik nach x
            GradPhi_coeff[k][0][0] = Phi_coeff[k][1] ;
            //Ableitung von Phik nach y
            GradPhi_coeff[k][1][0] = Phi_coeff[k][2] ;
            //Ableitung von Phik nach z
            GradPhi_coeff[k][2][0] = Phi_coeff[k][3] ;
            if(nGradPhi > 1){
                //Ableitung von Phik nach x
                GradPhi_coeff[k][0][1] = 2.0* Phi_coeff[k][4] ;
                GradPhi_coeff[k][0][2] = Phi_coeff[k][5] ;
                GradPhi_coeff[k][0][3] = Phi_coeff[k][7] ;
                //Ableitung von Phik nach y
                GradPhi_coeff[k][1][1] = Phi_coeff[k][5] ;
                GradPhi_coeff[k][1][2] = 2.0* Phi_coeff[k][6] ;
                GradPhi_coeff[k][1][3] = Phi_coeff[k][8] ;
                //Ableitung von Phik nach z
                GradPhi_coeff[k][2][1] = Phi_coeff[k][7] ;
                GradPhi_coeff[k][2][2] = Phi_coeff[k][8] ;
                GradPhi_coeff[k][2][3] = 2.0* Phi_coeff[k][9] ;
            }
            if(nGradPhi > 4){
                //Ableitung von Phik nach x
                GradPhi_coeff[k][0][4] = 3.0* Phi_coeff[k][10] ;
                GradPhi_coeff[k][0][5] = 2.0* Phi_coeff[k][11] ;
                GradPhi_coeff[k][0][6] = Phi_coeff[k][12] ;
                GradPhi_coeff[k][0][7] = 2.0* Phi_coeff[k][14] ;
                GradPhi_coeff[k][0][8] = Phi_coeff[k][15] ;
                GradPhi_coeff[k][0][9] = Phi_coeff[k][17] ;
                //Ableitung von Phik nach y
                GradPhi_coeff[k][1][4] = Phi_coeff[k][11] ;
                GradPhi_coeff[k][1][5] = 2.0* Phi_coeff[k][12] ;
                GradPhi_coeff[k][1][6] = 3.0* Phi_coeff[k][13] ;
                GradPhi_coeff[k][1][7] = Phi_coeff[k][15] ;
                GradPhi_coeff[k][1][8] = 2.0* Phi_coeff[k][16] ;
                GradPhi_coeff[k][1][9] = Phi_coeff[k][18] ;
                //Ableitung von Phik nach z
                GradPhi_coeff[k][2][4] = Phi_coeff[k][14] ;
                GradPhi_coeff[k][2][5] = Phi_coeff[k][15] ;
                GradPhi_coeff[k][2][6] = Phi_coeff[k][16] ;
                GradPhi_coeff[k][2][7] = 2.0* Phi_coeff[k][17] ;
                GradPhi_coeff[k][2][8] = 2.0* Phi_coeff[k][18] ;
                GradPhi_coeff[k][2][9] = 3.0* Phi_coeff[k][19] ;
            }

            delete VandermondeSP;
        }
    }
    if (verbose) std::cout << std::endl << std::endl;
}

double shape_functions::getPhi(const point *p, const int i) {
    // beruecksichtige IndexShift
    return polyval(&Phi_coeff[i], p) ;
}
//
//double shape_functions::getGradPhi_iGradPhi_j(double x, double y, int i, int j) {
//	double gradPhi_x_i, gradPhi_x_j, gradPhi_y_i, gradPhi_y_j ;
//
//	// Berechne die x-Komponente der Gradienten von Phi_i und Phi_y
//	gradPhi_x_i = polyval(&GradPhi_coeff[i][0], x, y) ;
//	gradPhi_x_j = polyval(&GradPhi_coeff[j][0], x, y) ;
//
//	// Berechne die y-Komponente der Gradienten von Phi_i und Phi_y
//    gradPhi_y_i = polyval(&GradPhi_coeff[i][1], x, y) ;
//    gradPhi_y_j = polyval(&GradPhi_coeff[j][1], x, y) ;
//
//    // Gib das Skalarprodukt zurueck
//    return gradPhi_x_i * gradPhi_x_j + gradPhi_y_i * gradPhi_y_j ;
//}

void shape_functions::getGradPhiI(const point *pin, const int i, point *pout)
{
    // Berechne die Komponenten der Gradienten von Phi_i und Phi_y
    for (int dim = 0; dim < GradPhi_coeff[0].size(); dim++)
    {
        pout->setXi(dim, polyval(&GradPhi_coeff[i][dim], pin));
    }
}

int shape_functions::getOrder() {return order ; }

int shape_functions::getnPhi() {return nPhi;}

double shape_functions::polyval(std::vector<double> *coeff, const point *p){

    int dimension = p->getDimension();
    // Rueckgabewert
    double value ;
    int nPhi = coeff->size();

    if (dimension == 1)
    {
        double x = p->getX();
        if (nPhi == 1){
            value =  (*coeff)[0];
        }
        // p1(x,y) = a0 + a1*x
        if (nPhi == 2){
            value =  (*coeff)[0] + (*coeff)[1]*x;
        }
        if (nPhi == 3){
            value =  (*coeff)[0] + (*coeff)[1]*x + (*coeff)[2]*x*x;
        }

    }
    if (dimension == 2)
    {
        double x = p->getX();
        double y = p->getY();
        if (nPhi == 1){
            value =  (*coeff)[0];
        }
        // p1(x,y) = a0 + a1*x + a2*y
        if (nPhi == 3){
            value =  (*coeff)[0] + (*coeff)[1]*x + (*coeff)[2]*y ;
        }
        // p2(x,y) = p1(x) + a3*xy + a4*x^2 + a5*y^2
        if (nPhi == 6){
            value = (*coeff)[0] + (*coeff)[1]*x + (*coeff)[2]*y + (*coeff)[3]*x*y + (*coeff)[4]*x*x + (*coeff)[5]*y*y ;
        }
        // p3(x,y) = p2(x) + a6*x^2*y + a7*x*y^2 + a8*x^3 + a9*y^3
        if (nPhi == 10){
            value = (*coeff)[0] + (*coeff)[1]*x + (*coeff)[2]*y + (*coeff)[3]*x*y + (*coeff)[4]*x*x + (*coeff)[5]*y*y
                + (*coeff)[6]*x*x*y + (*coeff)[7]*x*y*y + (*coeff)[8]*x*x*x + (*coeff)[9]*y*y*y ;
        }
    }
    if (dimension == 3)
    {
        double x = p->getXi(0);
        double y = p->getXi(1);
        double z = p->getXi(2);

        if (nPhi == 1){
            value = (*coeff)[0];
        }
        // p1(x,y,z) = a0 + a1*x + a2*y + a3*z
        if (nPhi == 4){
            value = (*coeff)[0] + (*coeff)[1]*x + (*coeff)[2]*y + (*coeff)[3]*z;
        }
        // p2(x,y,z) = p1(x,y,z) + a4*x^2 + a5*x*y + a6*y^2 + a7*x*z + a8*y*z + a9*z^2
        if (nPhi == 10){
            value = (*coeff)[0] + (*coeff)[1]*x + (*coeff)[2]*y + (*coeff)[3]*z + (*coeff)[4]*x*x + (*coeff)[5]*x*y + (*coeff)[6]*y*y
                + (*coeff)[7]*x*z + (*coeff)[8]*y*z + (*coeff)[9]*z*z;
        }
        // p3(x,y,z) = p2(x,y,z) + a10*x^3 + a11*x^2*y + a12*x*y^2 + a13*y^3 + a14*x^2*z + a15*x*y*z + a16*y^2*z
        //             + a17*x*z^2 + a18*y*z^2 + a19*z^3
        if (nPhi == 20){
            value = (*coeff)[0] + (*coeff)[1]*x + (*coeff)[2]*y + (*coeff)[3]*z + (*coeff)[4]*x*x + (*coeff)[5]*x*y + (*coeff)[6]*y*y
                + (*coeff)[7]*x*z + (*coeff)[8]*y*z + (*coeff)[9]*z*z + (*coeff)[10]*x*x*x + (*coeff)[11]*x*x*y + (*coeff)[12]*x*y*y
                + (*coeff)[13]*y*y*y + (*coeff)[14]*x*x*z + (*coeff)[15]*x*y*z + (*coeff)[16]*y*y*z + (*coeff)[17]*x*z*z
                + (*coeff)[18]*y*z*z + (*coeff)[19]*z*z*z;
        }
    }
    return value ;
}
