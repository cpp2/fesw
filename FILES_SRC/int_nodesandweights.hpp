#pragma once
#include <math.h>
#include <cstdlib>
#include <vector>
#include <iostream>

#include "math_macros.hpp"
#include "point.hpp"
#include "triangle_dunavant_rule.hpp"
#include "1d_quadrule.hpp"
#include "fastgl.hpp"
#include "tetrahedron_ncc_rule.hpp"

class int_nodesandweights{

private:
    int n; ///< Anzahl Stützstellen/Gewichte
    std::vector<point> x; ///< Stützstellen fuer Quadratur
    std::vector<double> w ; ///< Gewichte für Quadratur

public:
    // trivialer Konstruktor
    int_nodesandweights():n(0) {} ;

    ///Gibt Zeiger auf die Koordinatenliste zurück
    const std::vector<point> *getXlist() const;
    ///Gibt Zeiger auf bestimmte Stuetzstelle zurueck.
    const point * getXpoint(int index) const;
    ///Gibt Zeiger auf den Gewichtevektor zurueck.
    const std::vector<double> * getW() const;
    ///Gibt Anzahl der Stuetzstellen zurueck.
    int getNIntpoints() const;
    ///print nodes and weigts
    void print();

    /*!
     * this function takes the nodes and weights from
     *
     * John Burkardt
     *
     * under the GNU LGPL license.
     *
     * Reference:
     *
     *   1D:
     *
     *    Sylvan Elhay, Jaroslav Kautsky,
     *    Algorithm 655: IQPACK, FORTRAN Subroutines for the Weights of
     *    Interpolatory Quadrature,
     *    ACM Transactions on Mathematical Software,
     *    Volume 13, Number 4, December 1987, pages 399-415.
     *
     *    Roger Martin, James Wilkinson,
     *    The Implicit QL Algorithm,
     *    Numerische Mathematik,
     *    Volume 12, Number 5, December 1968, pages 377-383.
     *
     *
     *  2D:
     *
     *   David Dunavant,
     *   High Degree Efficient Symmetrical Gaussian Quadrature Rules
     *   for the Triangle,
     *   International Journal for Numerical Methods in Engineering,
     *   Volume 21, 1985, pages 1129-1148.
     *
     *
     *
     * @param rule number
     */
    void burkardtNodesAndWeightsWrapper(int dimension, int rule);
} ;



