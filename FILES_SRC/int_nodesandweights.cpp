#include "int_nodesandweights.hpp"


    /*!
     * Gibt Zeiger auf den Stuetzstellenvektor zurueck.
     */
    const std::vector<point> * int_nodesandweights::getXlist() const {return &x ; }
    const point * int_nodesandweights::getXpoint(int index) const {return &x[index] ; }
    const std::vector<double> * int_nodesandweights::getW() const {return &w ; }
    int int_nodesandweights::getNIntpoints() const {return n;}

void int_nodesandweights::print()
{
    std::cout << "NODESANDWEIGHTSPRINT: " << std::endl << "x: ";
    for(auto it: x)
        std::cout << "\t" << it.getX();
    //    cout << endl << "y: ";
    //    for(auto it: x)
    //        cout << "\t" << it.getY();
    std::cout << std::endl << "w: ";
    for(auto it: w)
        std::cout << "\t" << it;
    std::cout << std::endl;
}

void int_nodesandweights::burkardtNodesAndWeightsWrapper(int dimension, int rule)
{
    if (dimension ==1)
    {
        n = rule;

        for(int k = 1; k <= rule; k++)
        {
            fastgl::QuadPair p = fastgl::GLPair ( rule, k );

            x.push_back( (1.0 + p.x()) / 2.0 );
            w.push_back(p.weight / 2.0);
        }

    }
    if(dimension == 2)
    {
        point p_alloc(0.0,0.0);

        n=dunavant_order_num(dunavant_degree(rule));

        x.resize(n,p_alloc);
        w.resize(n);

        double *xyD = new double[2 * n];
        double *wD = new double[n];
        dunavant_rule ( dunavant_degree(rule), n, xyD, wD);

        LOOP(index,n){
            LOOP(coordI, dimension){

                x[index].setXi(coordI, xyD[coordI + index * dimension]);
                w[index] = wD[index] * 0.5;
            }
        }



        delete[] xyD;
        delete[] wD;
    }
    if (dimension == 3)
    {
        double *wtab;
        double *xyztab;

        n = tetrahedron_ncc_order_num ( rule );

        point p_alloc(0.0, 0.0, 0.0);

        x.resize(n,p_alloc);
        w.resize(n);

        xyztab = new double[dimension*n];
        wtab = new double[n];

        tetrahedron_ncc_rule ( rule, n, xyztab, wtab );

        LOOP(index,n){
            LOOP(coordI, dimension){

                x[index].setXi(coordI, xyztab[coordI + index * dimension]);
                w[index] = wtab[index];
            }
        }

        delete[] xyztab;
        delete[] wtab;
    }
}



