
#pragma once

#include <vector>

#include "myLib_wrapper.hpp"

#include "math_toolbox.hpp"
#include "denseMatrix.hpp"

#include "meshClass.hpp"
#include "transformationStuff.hpp"

#include "int_nodesandweights.hpp"
#include "shape_functions.hpp"
#include "specificFunctions.hpp"
#include "lgs.hpp"
#include "int_rule.hpp"



/*!
 \brief Enthält die nur während der Assemblierung benötigten Daten

 \version 2.0
 \author Lukas Duechting
 \date 2018

 */
struct assemblyTemp
{
public:
    int verbose;
    ///Datenstrukturen für Standard-Laplace
    denseMatrix Mref;
    std::vector<std::vector<double> > phiB;
    std::vector<std::vector<point> > phiGradK;

    ///Datenstrukturen für p-Laplace
    std::vector<double> bRef;

    /// sp-Matrix für die Steifigkeitsmatrixeinträge
    spMatrix matrixTempM;
    /// sp-Matrix für die Massematrixeinträge
    spMatrix matrixTempK;

    int_nodesandweights intK;
    int_nodesandweights intM;
    int_nodesandweights intB;

    shape_functions shapeFunctions;

    /// trivialer Konstruktor
    assemblyTemp();

    /// Konstruktor: belegt die für die Assemblierung nötigen Objekte; Integration, Basisfunktionen, etc
    assemblyTemp(int nodesandweightsK, int nodesandweightsM, int nodesandweightsB, meshClass &mesh);

    ///Berechne die Massenmatrix fuer das Referenzintervall
    void computeMref();
    ///Auswertung der Basisfunktionen an den Integrationsstuetzstellen, die durch nodesAndWeights vorgegeben werden
    void evaluatePhiAtIntPoints();
    ///Auswertung des Gradienten an den Integrationsstuetzstellen, die durch nodesAndWeights vorgegeben werden
    void evaluatePhiGradAtIntPoints();

    ///Berechne den Lastvektor fuer das Referenzintervall unter der Vorraussetzung f = 1
    void computeB_f1();
};


///Allokiere Speicher fuer *lgs und *mesh
void allocateLGS(LGS &lgs, meshClass &mesh);
///Assembliere die globalen Steifigkeits- und Massenmatrizen
int assembly(int nodesandweightsK, int nodesandweightsM, int nodesandweightsB, meshClass &mesh, LGS &lgs);

///Assembliere p-Laplace
int assemblyPLaplace(int verbose, assemblyTemp &assemblyT, int nodesandweightsK, int nodesandweightsM, int nodesandweightsB, meshClass &mesh, LGS &lgs, int p);

/// Wende die Dirichletbedingungen an
void applyDirichletConditions(meshClass &mesh, assemblyTemp &assemblyT, LGS &lgs);

/// Die sp-Matrizen in assemblyTemp sind bereits im vorraus allokiert, um eine parallele Berechnung in Assembly zu ermöglichen. Diese Funktion konvertiert element und node Indizes zu einem globalen Zugriffsindex
int indexConvertAssemblySP(int nodesPerEl, int elIndex, int nodeI, int nodeJ);

/*! Auswertung der RHS der PDE für die Assemblierung des Lastvektors
 */
void evaluateFAtGlobalPoints(double (*RHS_f)(point*), assemblyTemp &assemblyT, element *el, std::vector<double> &fGlobal);


