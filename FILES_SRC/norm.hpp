/*!
 * \file norm.hpp
 * \brief Berechnet Normen für einen gegebenen Vektor
 */
#pragma once

#include <iostream>
#include <vector>
#include <math.h>

#include "sparseMatrix.hpp"
#include "node.hpp"



/*!
 * Berechnet den Fehlervektor einer Funktion u.
 *
 * @param[in] *u Vektor dessen Fehler bestimmt werden soll
 * @param[out] Fehlervektor
 */
std::vector<double> computeError(std::vector<node> &x, double (*analyticalSolution)(point *p), std::vector<double> &u);


/*!
 * Berechnet die max-Norm einer Funktion u.
 *
 * @param[in] *u Vektor dessen Norm bestimmt werden soll
 * @param[out] norm Norm
 */
double max_norm(std::vector<double> &u);


/*!
 * Berechnet die H1-Norm einer Funktion u.
 *
 * @param[in] *u Vektor dessen H1-Norm bestimmt werden soll
 * @param[in] *K Steifigkeitsmatrix zur Berechnung der H1-Norm
 * @param[in] *M Massematrix zur Berechnung der H1-Norm
 * @param[out] norm H1-Norm
 */
double h1_norm(std::vector<double> &u, sparseMatrix &M, sparseMatrix &K);

/*!
 * Berechnet die H1-Seminorm einer Funktion u.
 *
 * @param[in] *u Vektor dessen H1-Seminorm bestimmt werden soll
 * @param[in] *K Steifigkeitsmatrix zur Berechnung der H1-Norm
 * @param[out] norm H1-Norm
 */
double h1_semi(std::vector<double> &u, sparseMatrix &K);


/*!
 * \brief Berechnet die L2-Norm einer Funktion u.
 * @param[in] Vektor u, dessen L2-Norm bestimmt werden soll.
 * @param[in] Massenmatrix M, wird zur Bestimmung der L2-Norm benoetigt.
 * @param[out] norm L2-Norm von Vektor u.
 */
double l2_norm(std::vector<double> &u, sparseMatrix &M);



