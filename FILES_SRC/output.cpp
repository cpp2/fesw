#include "output.hpp"


void outputToParaview(std::string filename, meshClass &mesh, LGS &lgs)
{
    std::ofstream file;

    const std::string Dateiname = "output/" + filename + ".csv" ;

    std::vector<node> nodelist = *mesh.getNodelist();

    file.open(Dateiname);
    if (mesh.getnDimension() == 1)
    {
        file << "X coord , u\n";
        LOOP(i, mesh.getnPoints() )
        {
            file << nodelist[i].getX() << "," << " \t " << lgs.u[i] << "\n" ;
        }
    }
    if (mesh.getnDimension() == 2)
    {
        file << "X coord   , Y coord , u\n";
        LOOP(i, mesh.getnPoints() )
        {
            file << nodelist[i].getX() << "," <<" \t " <<  nodelist[i].getY() << "," << " \t " << lgs.u[i] << "\n" ;
        }
    }
    if (mesh.getnDimension() == 3)
    {
        file << "X coord   , Y coord , Z coord , u\n";
        LOOP(i, mesh.getnPoints() )
        { // x und y holen wir uns direkt aus dem Gitter
            file << nodelist[i].getX() << "," <<" \t " <<  nodelist[i].getY() << "," <<" \t " <<  nodelist[i].getZ() << "," << " \t " << lgs.u[i] << "\n" ;
        }
    }
    file.close();

    LOG(info) << std::endl << "Paraview output written to " << Dateiname ;
}


void outputError(std::string filename, std::vector<double> error)
{
    std::ofstream file;
    const std::string Dateiname = "output/" + filename + "_err.txt" ;

    file.open(Dateiname);

    LOOP(i, error.size() )
    {
        file << error[i] << "\n" ;
    }

    file.close();
}
