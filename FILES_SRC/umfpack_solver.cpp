
#include "umfpack_solver.hpp"

int solve_umfpack(int rows, int cols, sparseMatrix *K, std::vector<double> *b, std::vector<double> *u)
{
    int status;
    void *Symbolic;
    void *Numeric;

    //
    //  Carry out the symbolic factorization.
    //
    status = umfpack_di_symbolic(rows, cols, K->getAp()->data(), K->getAi()->data(), K->getAx()->data(), &Symbolic, NULL, NULL);
    //
    //  Use the symbolic factorization to carry out the numeric factorization.
    //
    status = umfpack_di_numeric(K->getAp()->data(), K->getAi()->data(), K->getAx()->data(), Symbolic, &Numeric, NULL, NULL);
    //
    //  Free the memory associated with the symbolic factorization.
    //
    umfpack_di_free_symbolic (&Symbolic);
    //
    //  Solve the linear system.
    //
    status = umfpack_di_solve(UMFPACK_A, K->getAp()->data(), K->getAi()->data(), K->getAx()->data(), u->data(), b->data(), Numeric, NULL, NULL);
    //
    //  Free the memory associated with the numeric factorization.
    //
    umfpack_di_free_numeric (&Numeric);

    return status;
}



int solve_FEM_umfpack(LGS *lgs, meshClass *mesh)
{
    int status;
    void *Symbolic;
    void *Numeric;

    //
    //  Carry out the symbolic factorization.
    //
    status = umfpack_di_symbolic(mesh->getnPoints(), mesh->getnPoints(), lgs->K.getAp()->data(), lgs->K.getAi()->data(), lgs->K.getAx()->data(), &Symbolic, NULL, NULL);
    //
    //  Use the symbolic factorization to carry out the numeric factorization.
    //
    status = umfpack_di_numeric(lgs->K.getAp()->data(), lgs->K.getAi()->data(), lgs->K.getAx()->data(), Symbolic, &Numeric, NULL, NULL);
    //
    //  Free the memory associated with the symbolic factorization.
    //
    umfpack_di_free_symbolic (&Symbolic);
    //
    //  Solve the linear system.
    //
    status = umfpack_di_solve(UMFPACK_A, lgs->K.getAp()->data(), lgs->K.getAi()->data(), lgs->K.getAx()->data(), lgs->u.data(), lgs->b.data(), Numeric, NULL, NULL);
    //
    //  Free the memory associated with the numeric factorization.
    //
    umfpack_di_free_numeric (&Numeric);

    return status;
}

