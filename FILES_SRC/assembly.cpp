
#include "assembly.hpp"

assemblyTemp::assemblyTemp(){verbose = 0;};

assemblyTemp::assemblyTemp(int nodesandweightsK, int nodesandweightsM, int nodesandweightsB, meshClass &mesh)
:matrixTempM(mesh.getnPoints(), mesh.getnPoints()), matrixTempK(mesh.getnPoints(), mesh.getnPoints())
{
    this->verbose = 1;

    intK = int_nodesandweights();
    intK.burkardtNodesAndWeightsWrapper(mesh.getnDimension(), nodesandweightsK);
    intM = int_nodesandweights();
    intM.burkardtNodesAndWeightsWrapper(mesh.getnDimension(), nodesandweightsM);
    intB = int_nodesandweights();
    intB.burkardtNodesAndWeightsWrapper(mesh.getnDimension(), nodesandweightsB);


    meshClass refElMesh;

    if (mesh.getnOrder() == 2 && mesh.getnDimension() == 3)
    {
        refElMesh.defineRefElement_SecondOrder(mesh.getnDimension());
    }
    else
    {
        refElMesh.defineRefElementFirstOrder(mesh.getnDimension());
        refElMesh.addHighOrderPoints(mesh.getnOrder());
    }


    shapeFunctions.compute_shape_functions(0, refElMesh);

    int nBasisPoints = shapeFunctions.getnPhi();
    int nEntriesK = mesh.getnElements() * nBasisPoints * nBasisPoints;

    matrixTempK.allocateEntries(nEntriesK);
    matrixTempM.allocateEntries(nEntriesK);
}


void assemblyTemp::computeMref()
{
    int nPhi = shapeFunctions.getnPhi();
    int nIntPoints = intM.getNIntpoints();

    Mref.resize(nPhi, nPhi);

    std::vector<double> valueTemp;
    valueTemp.resize(nIntPoints);
    std::vector<std::vector<double> > phiEvaluation;

    LOOP(phiI, nPhi){

        phiEvaluation.push_back(std::vector<double>());

        LOOP(intPoint, nIntPoints){

            phiEvaluation[phiI].push_back(shapeFunctions.getPhi(intM.getXpoint(intPoint), phiI));
        }
    }

    LOOP(phiI, nPhi){
        LOOP(phiJ, nPhi){

            pointwiseArrayMultiplication(phiEvaluation[phiI], phiEvaluation[phiJ], valueTemp);
            Mref.setEntry(phiI, phiJ, integral_ref(valueTemp, *intM.getW()));
        }
    }

}

void assemblyTemp::computeB_f1()
{
    int nPhi = shapeFunctions.getnPhi();
    int nIntPoints = intB.getNIntpoints();

    bRef.resize(nPhi);

    std::vector<std::vector<double> > phiEvaluation;

    LOOP(phiI, nPhi){

        phiEvaluation.push_back(std::vector<double>());

        LOOP(intPoint, nIntPoints){

            phiEvaluation[phiI].push_back(shapeFunctions.getPhi(intM.getXpoint(intPoint), phiI));
        }
    }

    LOOP(phiI, nPhi){

        bRef[phiI] = integral_ref(phiEvaluation[phiI], *intM.getW());
    }
}

void assemblyTemp::evaluatePhiAtIntPoints()
{
    int nPhi = shapeFunctions.getnPhi();
    int nIntPoints = intB.getNIntpoints();

    phiB.resize(nPhi);

    LOOP(phiI, nPhi){
        phiB[phiI].resize(nIntPoints);

        LOOP(intPointI, nIntPoints){

            phiB[phiI][intPointI] = shapeFunctions.getPhi(intB.getXpoint(intPointI), phiI);
        }
    }
}

void assemblyTemp::evaluatePhiGradAtIntPoints()
{
    int nPhi = shapeFunctions.getnPhi();
    int nIntPoints = intK.getNIntpoints();

    phiGradK.resize(nPhi);

    LOOP(phiI, nPhi){
        phiGradK[phiI].resize(nIntPoints);
        LOOP(intPointI, nIntPoints){

            shapeFunctions.getGradPhiI(intK.getXpoint(intPointI), phiI, &phiGradK[phiI][intPointI]);
        }
    }
}

#define DEBUG if(0) LOG(debug)

int assembly(int nodesandweightsK, int nodesandweightsM, int nodesandweightsB, meshClass &mesh, LGS &lgs)
{
    bool computeFullKMatrix = false;


    LOG(debug) << std::endl << "ASSEMBLY";

    // Allocate lgs arrays
    allocateLGS(lgs, mesh);

    // Allocate temporary stuff and set shapeFunctions, nodesandweights etc
    assemblyTemp assemblyT(nodesandweightsK, nodesandweightsM, nodesandweightsB, mesh);

    assemblyT.computeMref();
    assemblyT.evaluatePhiAtIntPoints();
    assemblyT.evaluatePhiGradAtIntPoints();

    //    clock_t timer_var_estimate_assembly = clock();
    TIMER_LOCAL_INIT
    int estimateTimeElementIndex = 50;

    volumeElement *el;
    transformationStuff *trans;
    std::vector<node> *nodelist = mesh.getNodelist();
    double absDet;
    int nShapeFunctions = assemblyT.shapeFunctions.getnPhi();
    int globalRow, globalCol;

    // Für den Lastvektor
    std::vector<double> fGlobal, phiTimesF;
    fGlobal.resize(assemblyT.intB.getNIntpoints());
    phiTimesF.resize(assemblyT.intB.getNIntpoints());

    //    LOOP(element, mesh.getnElements()){

    int elementIndex;

//#pragma omp parallel for shared(lgs, mesh, assemblyT,nodelist,nShapeFunctions) private(elementIndex, fGlobal, phiTimesF, globalRow, globalCol, el, trans)
    for (elementIndex = 0; elementIndex < mesh.getnElements(); elementIndex++)
    {

        el = mesh.getElement(elementIndex);
        trans = el->getTransformation();
        absDet = fabs(trans->getB_T()->getDet());


        if (elementIndex == estimateTimeElementIndex)
        {
            TIMER_LOCAL_STOP
            LOG(info) << std::endl << "ESTIMATED TIME TO COMPUTE ASSEMBLY ENTRIES: " << TIMER_LOCAL_splitTime * mesh.getnElements() / estimateTimeElementIndex;
        }



        if (lgs.reqData.K)
        {
            std::vector<point> *gradPhiI, *gradPhiJ;
            std::vector<double> gradPhiI_BTB_gradPhiJ;
            gradPhiI_BTB_gradPhiJ.resize(assemblyT.intK.getNIntpoints());

            LOOP(phiI, nShapeFunctions){

                globalRow = mesh.getGlobalNodeIndexFromElement(elementIndex, phiI);
                gradPhiI = &assemblyT.phiGradK[phiI];

                DEBUG << "PHI " << phiI << " global " << globalRow;

                LOOP(phiJ, nShapeFunctions){

                    globalCol = mesh.getGlobalNodeIndexFromElement(elementIndex, phiJ);
                    gradPhiJ = &assemblyT.phiGradK[phiJ];

                    DEBUG << "phi " << phiJ << " global " << globalCol ;

                    if (computeFullKMatrix || !(*nodelist)[globalRow].getBoundaryInfo())
                    {

                        LOOP(i, assemblyT.intK.getNIntpoints()){
                            if (0){
                                DEBUG << "gradPhi_gradphi int point: " << i << " phiI: " << (*gradPhiI)[i] ;
                                (*trans->getBTB()).print();
                            }
                            gradPhiI_BTB_gradPhiJ[i] = (*gradPhiI)[i]*((*trans->getBTB())*(*gradPhiJ)[i]);
                        }

                        assemblyT.matrixTempK.setEntry(indexConvertAssemblySP(nShapeFunctions, elementIndex, phiI, phiJ), globalRow, globalCol, absDet * integral_ref(gradPhiI_BTB_gradPhiJ, *assemblyT.intK.getW()));

                    }
                }
            }
        }
    }

    for (elementIndex = 0; elementIndex < mesh.getnElements(); elementIndex++)
    {

        el = mesh.getElement(elementIndex);
        trans = el->getTransformation();
        absDet = fabs(trans->getB_T()->getDet());

        if (lgs.reqData.b)
        {
            evaluateFAtGlobalPoints(lgs.RHS_f, assemblyT, el, fGlobal);

            LOOP(phiI, nShapeFunctions){

                globalRow = mesh.getGlobalNodeIndexFromElement(elementIndex, phiI);

                pointwiseArrayMultiplication(fGlobal, assemblyT.phiB[phiI], phiTimesF);

                DEBUG << "PHI " << phiI << " global " << globalRow \
                    << " integral beitrag: " << absDet * integral_ref(phiTimesF, *assemblyT.intB.getW());

                lgs.b[globalRow] += absDet * integral_ref(phiTimesF, *assemblyT.intB.getW());
            }
        }
    }


    applyDirichletConditions(mesh, assemblyT, lgs);

    assemblyT.matrixTempK.reduce();

    lgs.K.fillFromSp(&assemblyT.matrixTempK);

    return EXIT_SUCCESS;
}



int assemblyPLaplace(int verbose, assemblyTemp &assemblyT, int nodesandweightsK, int nodesandweightsM, int nodesandweightsB, meshClass &mesh, LGS &lgs, int p)
{
    bool computeFullJacobi = false;


    if(verbose)
        LOG(debug) << std::endl << "ASSEMBLY" << std::endl;

    spRep spRepTemp = spRep();

    //resette die temporäre spMatrix für die Steifigkeitsmatrix vor dem nächsten Iterationsschritt
    assemblyT.matrixTempK.getEntries()->assign(mesh.getnElements() * assemblyT.shapeFunctions.getnPhi() * assemblyT.shapeFunctions.getnPhi(), spRepTemp);

    //resette den Lastvektor, der hier die Auswertung der Funktion F des Newton-Verfahrens trägt
    lgs.b.assign(lgs.b.size(), 0.0);

    //Berechne den Referenzelement Lastvektor
    assemblyT.computeB_f1();
    //Werte phiGrad aus
    assemblyT.evaluatePhiGradAtIntPoints();

    //    clock_t timer_var_estimate_assembly = clock();
    TIMER_LOCAL_INIT

    volumeElement *el;
    transformationStuff *trans;
    std::vector<node> *nodelist = mesh.getNodelist();
    double absDet;
    int nShapeFunctions = assemblyT.shapeFunctions.getnPhi();
    int nIntPoints = assemblyT.intK.getNIntpoints();
    int globalRow, globalCol;

    int elementIndex;

    //Die Parallelisierung macht leider noch ein paar Probleme; für 3D kommt manchmal das Falsche Ergebnis
#pragma omp parallel for shared(lgs, mesh, assemblyT) private(elementIndex, globalRow, globalCol, el, trans)
    for (elementIndex = 0; elementIndex < mesh.getnElements(); elementIndex++)
    {

        el = mesh.getElement(elementIndex);
        trans = el->getTransformation();
        absDet = fabs(trans->getB_T()->getDet());
        denseMatrix2 B_T1 = trans->getB_T()->getInverse();


        std::vector<point> *gradPhiI, *gradPhiJ;
        std::vector<double> gradPhiI_BTB_gradPhiJ;
        gradPhiI_BTB_gradPhiJ.resize(assemblyT.intK.getNIntpoints());

        std::vector<std::vector<double> > gradU_BTB_gradPhiJ;
        gradU_BTB_gradPhiJ.resize(nShapeFunctions);
        LOOP(i, nShapeFunctions)
        gradU_BTB_gradPhiJ[i].resize(nIntPoints);

        std::vector<point> uGrad;
        point pointTemp = point(mesh.getnDimension());
        uGrad.resize(nIntPoints, pointTemp);

        std::vector<double> temp;
        temp.resize(nIntPoints);


        std::vector<double> uGradNorm;
        uGradNorm.resize(nIntPoints);


        //Bestimme den Gradienten von u, grad(u) * B^-T * B^-1 * grad(phi) und die Norm von grad(u)
        LOOP(intPointIndex, nIntPoints){

            LOOP(phiI, nShapeFunctions){

                globalRow = mesh.getGlobalNodeIndexFromElement(elementIndex, phiI);
                LOOP(i, uGrad[0].getDimension())
                uGrad[intPointIndex].setXi(i, (assemblyT.phiGradK[phiI][intPointIndex].getXi(i) * lgs.u[globalRow]));
            }
            LOOP(phiI, nShapeFunctions){

                gradU_BTB_gradPhiJ[phiI][intPointIndex] = uGrad[intPointIndex] * (*trans->getBTB() * assemblyT.phiGradK[phiI][intPointIndex]);
            }
            uGrad[intPointIndex] = B_T1*uGrad[intPointIndex];
            uGradNorm[intPointIndex] = sqrt(uGrad[intPointIndex] * uGrad[intPointIndex]);
        }

        LOOP(phiI, nShapeFunctions){

            globalRow = mesh.getGlobalNodeIndexFromElement(elementIndex, phiI);
            gradPhiI = &assemblyT.phiGradK[phiI];

            std::vector<double> uGradNormSqr;
            uGradNormSqr.resize(nIntPoints);
            LOOP(i,nIntPoints)
            uGradNormSqr[i] = pow(uGradNorm[i], (double)(p-2));

            pointwiseArrayMultiplication(uGradNormSqr, gradU_BTB_gradPhiJ[phiI], temp);

            // F aus dem Newtonverfahren ist definiert wie in der Ausarbeitung und wird hier an lgs.u ausgewertet
            lgs.b[globalRow] += absDet * (integral_ref(temp , *assemblyT.intB.getW())-assemblyT.bRef[phiI]);


            LOG(debug) << "PHI " << phiI << " global " << globalRow << std::endl;

            LOOP(phiJ, nShapeFunctions){

                globalCol = mesh.getGlobalNodeIndexFromElement(elementIndex, phiJ);
                gradPhiJ = &assemblyT.phiGradK[phiJ];

                LOG(debug) << "phi " << phiJ << " global " << globalCol << std::endl;

                if (computeFullJacobi || !(*nodelist)[globalRow].getBoundaryInfo())
                {

                    LOOP(i, nIntPoints){

                        gradPhiI_BTB_gradPhiJ[i] = (*gradPhiI)[i]*((*trans->getBTB())*(*gradPhiJ)[i]);
                        temp[i] = pow(uGradNorm[i], (double)(p-4));
                    }

                    /*
    cout << endl << "K: row: " << globalRow << " col: " << globalCol << "integral: " << absDet * integral_ref( (double)(p-2) * pointwiseArrayMultiplication(temp, pointwiseArrayMultiplication(gradU_BTB_gradPhiJ[phiI], gradU_BTB_gradPhiJ[phiJ])) \
        + pointwiseArrayMultiplication(gradPhiI_BTB_gradPhiJ, uGradNormSqr), *assemblyT.intK.getW()) << endl;

    cout << absDet << endl;
    cout << (p-2) << endl;
    PRINT_VECTOR(lgs.u);
    PRINT_VECTORVECTOROBJ(assemblyT.phiGradK)
    cout  << endl << "asdf" << endl;
    PRINT_VECTOR(uGrad)
    cout << endl;
    PRINT_VECTOR(uGradNorm)
    cout << endl;
    PRINT_VECTOR(temp)
    cout << endl;
    PRINT_VECTORVECTOR(gradU_BTB_gradPhiJ)
    cout  << endl;
    PRINT_VECTOR(gradPhiI_BTB_gradPhiJ)
    cout << endl;
    PRINT_VECTOR(uGradNormSqr)
    cout << endl;
*/
                    //Bestimme die Jacobi aus dem Newtonverfahren wie in der Ausarbeitung
                    assemblyT.matrixTempK.setEntry(indexConvertAssemblySP(nShapeFunctions, elementIndex, phiI, phiJ), \
                        globalRow, globalCol, \
                        absDet * integral_ref( (double)(p-2) * pointwiseArrayMultiplication(temp, pointwiseArrayMultiplication(gradU_BTB_gradPhiJ[phiI], gradU_BTB_gradPhiJ[phiJ])) \
                            + pointwiseArrayMultiplication(gradPhiI_BTB_gradPhiJ, uGradNormSqr), *assemblyT.intK.getW()));

                }
            }
        }
    }



    // Wende Dirichletbedingungen auf lgs.u an
    LOOP(pointIndex, mesh.getnPoints()){
        if ((*nodelist)[pointIndex].isDirichlet())
            lgs.u[pointIndex] = lgs.BC_dirichlet(&(*nodelist)[pointIndex]);
    }

    applyDirichletConditions(mesh, assemblyT, lgs);

    assemblyT.matrixTempK.reduce();

    lgs.K.fillFromSp(&assemblyT.matrixTempK);

    return EXIT_SUCCESS;
}


int indexConvertAssemblySP(int nodesPerEl, int elIndex, int nodeI, int nodeJ)
{
    return (elIndex)*nodesPerEl*nodesPerEl + nodeI*nodesPerEl + nodeJ;
}


void applyDirichletConditions(meshClass &mesh, assemblyTemp &assemblyT, LGS &lgs)
{
    int nPoints = mesh.getnPoints();
    spMatrix kTemp(nPoints, nPoints);

    /*************** dirichlet Bedingungen ************/

    std::vector<node> *nodelist = mesh.getNodelist();

    int pointIndexI, pointIndexJ;

    // Wende die Dirichletbedingungen auf den Lastvektor an
    LOOP(pointIndex, mesh.getnPoints()){
        if ((*nodelist)[pointIndex].isDirichlet())
            lgs.b[pointIndex] = lgs.BC_dirichlet(&(*nodelist)[pointIndex]);
    }

    for (auto spEntry : *assemblyT.matrixTempK.getEntries())
    {
        pointIndexI = spEntry.row;
        pointIndexJ = spEntry.column;

        // Füge die Kii-Einträge komplett zu K hinzu
        if(!(*nodelist)[pointIndexI].isDirichlet() && !(*nodelist)[pointIndexJ].isDirichlet())
            kTemp.insertEntry(pointIndexI, pointIndexJ, spEntry.value);

        // Wende die Kid Dirichletbeiträge auf den Lastvektor an
        if(!(*nodelist)[pointIndexI].isDirichlet() && (*nodelist)[pointIndexJ].isDirichlet())
            lgs.b[pointIndexI] -= spEntry.value * lgs.b[pointIndexJ];
    }

    // Füge die Kdd-Einträge (Kdd = Einheitsmatrix) zu K hinzu
    LOOP(pointIndexI, nPoints){

        if ((*nodelist)[pointIndexI].isDirichlet())
            kTemp.insertEntry(pointIndexI, pointIndexI, 1.0);
    }

    assemblyT.matrixTempK.getEntries()->swap(*kTemp.getEntries());

}


void allocateLGS(LGS &lgs, meshClass &mesh)
{
    if (lgs.reqData.u)
    {
        lgs.u.resize(mesh.getnPoints(), 0.1);
    }
    if (lgs.reqData.ut)
    {
        lgs.ut.resize(mesh.getnPoints());
    }
    if (lgs.reqData.b)
    {
        lgs.b.resize(mesh.getnPoints(), 0.0);
    }
    if (lgs.reqData.M)
    {
        lgs.M.resize(mesh.getnPoints(),mesh.getnPoints());
    }
    if (lgs.reqData.K)
    {
        lgs.K.resize(mesh.getnPoints(), mesh.getnPoints());
    }
}


void evaluateFAtGlobalPoints(double (*RHS_f)(point*), assemblyTemp &assemblyT, element *el, std::vector<double> &fGlobal)
{
    int_nodesandweights *nodesandweights = &assemblyT.intB;
    int n = nodesandweights->getNIntpoints();
    point xGlobal;
    transformationStuff *trans = el->getTransformation();


    LOOP(intPoint, n){

        trans->transform(*nodesandweights->getXpoint(intPoint), xGlobal);

        fGlobal[intPoint]=(*RHS_f)(&xGlobal);
    }
}


