
#include "specificFunctions.hpp"


double dir0(point *p)
{
    return 0.0;
}

double dir1(point *p)
{
    return 1.0;
}

bool isNeumann(point *p)
{
    return false;
}


double rhs_function_testgrid_sin(point *x)
{
    if (x->getDimension() == 1)
    {
        return 1.0 * M_PI * M_PI * sin(M_PI * x->getX());
    }
    else if (x->getDimension() == 2)
    {
        return 2.0 * M_PI * M_PI * sin(M_PI * x->getX()) * sin(M_PI * x->getY());
    }
    else if (x->getDimension() == 3)
    {
        return 3.0 * M_PI * M_PI * sin(M_PI * x->getX()) * sin(M_PI * x->getY()) * sin(M_PI * x->getZ());
    }
    return 0;

}


double rhs_f1(point *x)
{
    return 1.0;
}

double u_exact_testgrid_1(point *xP)
{
    if (xP->getDimension() == 1)
    {
        double x = xP->getX();
        return 0.5*((x-1)*x);
    }
    else if (xP->getDimension() == 2)
    {
        return 0.0;
    }
    else if (xP->getDimension() == 3)
    {
        return 0.0;
    }
    return 0;

}

double u_exact_testgrid_sin(point *x)
{
    if (x->getDimension() == 1)
    {
        return sin(M_PI * x->getX());
    }
    else if (x->getDimension() == 2)
    {
        return sin(M_PI * x->getX()) * sin(M_PI * x->getY());
    }
    else if (x->getDimension() == 3)
    {
        return sin(M_PI * x->getX()) * sin(M_PI * x->getY()) * sin(M_PI * x->getZ());
    }
    return 0;

}

