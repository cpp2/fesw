

set(CMAKE_VERBOSE_MAKEFILE 0 CACHE INTERNAL "")
set(_LIB_TYPE STATIC)
set(INCLUDE_PATH "/home/lukas/CODE/cpp/cpp_libs/include")

FUNCTION(CMAKE_CUSTOM)

    findlib(FELIB felib ${INCLUDE_PATH})
    list(APPEND _ADD_LIBS ${FELIB})

    findlib(MATRIXLIB matrixlib ${INCLUDE_PATH})
    list(APPEND _ADD_LIBS ${MATRIXLIB})

    findlib(MYLIB mylib_cpp "${INCLUDE_PATH}")
    list(APPEND _ADD_LIBS ${MYLIB})

    findlib(BOOST Boost " " log timer program_options)
    list(APPEND _ADD_LIBS ${BOOST})

    findlib(UMFPACK umfpack "PATH")
    list(APPEND _ADD_LIBS ${UMFPACK})

    message(STATUS "Linked Libs are: ${_ADD_LIBS}")

    add_subdirectory(FILES_MAIN)
    add_subdirectory(TEST)

ENDFUNCTION()
