#include <iostream>
#include <cstdlib>
#include <vector>
#include <math.h>
#include <math_macros.hpp>
#include <fstream>
#include <string>

#include "point.hpp"
#include "element.hpp"
#include "meshClass.hpp"

#include "../FILES_SRC/shape_functions.hpp"
#include "../FILES_SRC/specificFunctions.hpp"
#include "../FILES_SRC/assembly.hpp"

using namespace std ;

/** \file test_transformBackAndTestShape.cpp
 * Hier sollen die verschiedenen Basisfunktionen, Transformationen und das Mesh getestet werden.
 * Dafür werden die globalen Koordinaten auf das Referenzelement ruecktransformiert, und fuer jedes Element,
 * jede Basisfunktion auf jedem lokalen Punkt auf die Kroneckereigenschaft getestet.
 */
int main(int argc, char **argv)
{
    auto vm = myLib::parseCommandlineArguments(argc, argv);
    auto log_options = myLib::parseLoggerArguments(vm);
    myLib::initLogger(log_options);

    LOG(trace) << "Start program";
    
    int verbose = true;
    int DIMENSION = 3;
    int n = 3;
    int order = 2;

    meshClass mesh;
    mesh.setDimension(DIMENSION);
    mesh.createTestMesh(n);
    mesh.addHighOrderPoints(order);

    meshClass refElMesh;

    if (mesh.getnOrder() == 2 && mesh.getnDimension() == 3)
    {
        refElMesh.defineRefElement_SecondOrder(mesh.getnDimension());
    }
    else
    {
        refElMesh.defineRefElementFirstOrder(mesh.getnDimension());
        refElMesh.addHighOrderPoints(mesh.getnOrder());
    }


//    mesh.print();

    point *pTemp = new point();

    assemblyTemp assemblyT(1, 1, 1, mesh);


    int nElements = mesh.getnElements();
    int nPhi = assemblyT.shapeFunctions.getnPhi();
    shape_functions *Phi = &assemblyT.shapeFunctions;
    vector<node> *nodelist = refElMesh.getNodelist();


    bool testPassed = true;

    LOOP(element, nElements){

        transformationStuff * transstuff = mesh.getElement(element)->getTransformation();

        LOOP(phiI, nPhi){

            LOOP(localPoint, nPhi){

                transstuff->transform_back( *mesh.getGlobalNodeFromElement(element, localPoint) , *pTemp);


                if (phiI == localPoint && !ISZERO(Phi->getPhi(pTemp, phiI)-1))
                {
                    testPassed = false;
                    cout << endl << "DAS SOLLTE 1 SEIN" << endl;
                    cout << "Teste " << element << "-tes Element:" << endl  ;
                    cout << "Teste " << phiI << "-te Basisfunktion:" << endl  ;
                    cout << "Am " << localPoint << "ten lokalen Punkt haben wir ";
                    cout << Phi->getPhi(pTemp, phiI) << endl ;
                    cout << "coord phys: " << *mesh.getGlobalNodeFromElement(element, localPoint) << "   coord transformed back: " << *pTemp;
                    transstuff->transform(nodelist->at(localPoint), *pTemp);
                    cout << "ref El coord: " << nodelist->at(localPoint) << "coord transformed to phys: " << *pTemp << endl;
                    cout << endl;
                }
                if (phiI != localPoint && !ISZERO(Phi->getPhi(pTemp, phiI)))
                {
                    testPassed = false;
                    cout << endl << "DAS SOLLTE 0 SEIN" << endl;
                    cout << "Teste " << element << "-tes Element:" << endl  ;
                    cout << "Teste " << phiI << "-te Basisfunktion:" << endl  ;
                    cout << "Am " << localPoint << "ten lokalen Punkt haben wir ";
                    cout << Phi->getPhi(pTemp, phiI) << endl ;
                    cout << "   coord phys: " << *mesh.getGlobalNodeFromElement(element, localPoint) << "   coord transformed back: " << *pTemp;
                    transstuff->transform(nodelist->at(localPoint), *pTemp);
                    cout << "ref El coord: " << nodelist->at(localPoint) << "coord transformed to phys: " << *pTemp << endl;
                    cout << endl;
                }
            }
        }
    }

    cout << endl << "TEST ORDER " << mesh.getnOrder() << " PASSED: " << testPassed << endl << endl;


    return 0 ;
}
