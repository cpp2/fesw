
/*! \file test_conjugateGradient.cpp
 \brief Test fuer die Konjugierte Gradienten Methode
 Fuehrt das CG-Verfahren sowie das PCG-Verfahren an einem einfachen Beispiel (vgl.
 https://de.wikipedia.org/wiki/CG-Verfahren ) durch.
 */

#include <stdio.h>
#include <vector>
#include <dirent.h>
#include <sys/stat.h>
#include <fstream>
#include <omp.h>
#include <random>
#include <cmath>

#include "myLib_wrapper.hpp"

#include "matrix.hpp"
#include "denseMatrix.hpp"
#include "denseMatrix2.hpp"
#include "sparseMatrix.hpp"
#include "denseMatrix.hpp"

//#include "../FILES_SRC/toolbox.hpp"
#include "../FILES_SRC/conjugateGradient.hpp"


/** TESTFUNCTION
 * Fuehrt die Konjugierte Gradienten Methode an einem einfachen Beispiel durch
\date 2018
\version 1.0

 */
int main(int argc, char **argv)
{
    auto vm = myLib::parseCommandlineArguments(argc, argv);
    auto log_options = myLib::parseLoggerArguments(vm);
    myLib::initLogger(log_options);

    LOG(trace) << "Start program";
  
    int verbose = 1;

    denseMatrix Atemp(2,2);
    sparseMatrix A;
    std::vector<double> x;
    std::vector<double> b;

    // Erstelle dense-Matrix
    Atemp.setEntry(0,0,4);
    Atemp.setEntry(0,1,1);
    Atemp.setEntry(1,0,1);
    Atemp.setEntry(1,1,3);

    std::cout << "Systemmatrix A: " << std::endl;
    Atemp.print();

    // Fuelle A von denseMatrix Atemp
    A.fillFromDense(&Atemp);
    // Rechte Seite b
    b.resize(2);
    b[0] = 1.0;
    b[1] = 2.0;

    std::cout << std::endl << "Rechte Seite b: " << std::endl;
    PRINT_VECTOR(b);
    std::cout << std::endl;

    //_____________________ TEST FUER CG-VERFAHREN_________________________________

    std::cout << std::endl << " \t \t TESTE CG-VERFAHREN:" << std::endl << std::endl;

    conjugateGradient(verbose,A,b,x);

    std::cout << "Loesung : \t";
    PRINT_VECTOR(x);
    std::cout << std::endl;

    //_______________________ ENDE ________________________________________________

    //_____________________ TEST FUER PCG-VERFAHREN_________________________________
    std::cout << std::endl << "\t \t TESTE PCG-VERFAHREN:" << std::endl << std::endl;
    PrecConjugateGradient(verbose, A,b,x,"jacobi");
    std::cout << "Loesung : \t";
    PRINT_VECTOR(x);
    std::cout << std::endl;

    //______________________ ENDE _________________________________________________

    // _________________________ TEST FUER DIAG-FKT _____________________________

    std::cout << std::endl << "\t \t TEST FUER DIAG-FKT:" << std::endl;

    std::random_device r;
    std::default_random_engine e1(r());
    std::uniform_real_distribution<double> uniform_dist(1, 6);

    std::default_random_engine e2(r());
    std::uniform_int_distribution<int> uniform_dist_int(0, 3);

    int n = 10;

    denseMatrix m(n, n);

    for (int i = 0; i<n; i++){
        for (int j = 0; j<n; j++){
            if (uniform_dist_int(e2)==0)
                m.setEntry(i,j,uniform_dist(e1));
        }
    }

    if (n<20)
        std::cout << "Matrix A: " ;
    m.print();

    sparseMatrix temp;
    temp.fillFromDense(&m);

    //    temp.print();

    sparseMatrix D = temp.diag();
    //    cout << "diag(A): ";
    //    D.print();

    //_____________________________ ENDE _____________________________________

    //____________________________ TEST FUER BLOCKDIAGONAL-MATRIX _______________________
    std::cout << std::endl << "\t \t TEST FUER BLOCKDIAGONAL-MATRIX: \n" << std::endl;
    int blockSize = 3;
    int blockSize2 = m.getRows() % blockSize;
    sparseMatrix temp2;
    temp2.fillFromDense(&m);
    std::vector<sparseMatrix> blocklist = temp2.blockdiag(blockSize);

//    LOOP(blockNumber,blocklist.size()){
//
//        cout << endl << blockNumber + 1 << "-ter Block: " << endl;
//        blocklist[blockNumber].print();
//        cout << endl;
//    }


    denseMatrix printMatrix2;

    LOOP(blockNumber,blocklist.size() -1){
        denseMatrix printMatrix;
        // Wandle in denseMatrix um
        printMatrix.fillFromSparse(blocklist[blockNumber]);
        std::cout << std::endl << blockNumber + 1 << "-ter Block: " << std::endl;
        printMatrix.print();
        std::cout << std::endl;
        printMatrix.resize(blockSize,blockSize);
    }
//  Printe die letzte Matrix separat
    printMatrix2.fillFromSparse(blocklist[blocklist.size()-1]);
    std::cout << std::endl << "Letzter Block: " << std::endl;
    printMatrix2.print();
    std::cout << std::endl;

    //___________________________     ENDE _______________________________________________

    //__________________________ TEST FUER BLOCKJACOBI-PRECONDITIONER _____________________

    std::cout << std::endl << " \t \t TEST FUER BLOCKJACOBI-PRECONDITIONER: \n" << std::endl;
//    vector<double> RHS(n,0.0);

//    LOOP(i,n){
//        RHS[i] = 0.0;
//    }

    PrecConjugateGradient(verbose,A,b,x,"blockjacobi",1);
    // __________________________________ ENDE ___________________________________________-
    return 0;
}
