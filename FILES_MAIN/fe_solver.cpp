/*-------------------------------------------
|(c) Alexander Heinlein, Martin Lanser, 2018|
---------------------------------------------*/
// TODO
/**
 \mainpage
 Löse 3D Poisson Gleichung auf \f$\Omega = [0,1]^3\f$ <br><br>
 \f$
 \begin{array}{lll}
 -\Delta u &= 1  &x\in \Omega\\
u &= 0  &x\in\partial\Omega\\
\end{array}
 \f$<br><br>
oder die \f$p\f$-Laplace Gleichung <br><br>
\f$
\begin{array}{lll}
-\Delta_p u &= 1 &x\in \Omega\\
u &= 0 &x\in \partial\Omega.\\
\end{array}
 \f$<br><br>

 <br><br>
 Man kann verschiedene Parameteroptionen aufrufen, indem man das Programm mit \-\-help aufruft

 \-\-filename<br>
 \-\-testmesh<br>
 \-\-order<br>
 \-\-verbose<br>
 \-\-output<br>

<br>
Unter den Files finden sich zahlreiche Tests zu verschiedenen Funktionalitaeten.
 */


/**
 * \file fe_solver.cpp
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>


#include <myLib_wrapper.hpp>

#include <meshClass.hpp>
#include <math_macros.hpp>

#include "../FILES_SRC/lgs.hpp"
#include "../FILES_SRC/specificFunctions.hpp"
#include "../FILES_SRC/assembly.hpp"
#include "../FILES_SRC/conjugateGradient.hpp"
#include "../FILES_SRC/newton.hpp"
#include "../FILES_SRC/norm.hpp"
#include "../FILES_SRC/output.hpp"



using std::string;
using std::endl;



boost::program_options::options_description getProgramArguments()
{
    auto desc = myLib::getLoggingProgramArguments();

    desc.add_options()
        ("order,o", boost::program_options::value<int>()->default_value(1), "Choose order") \
        ("problem,p", boost::program_options::value<int>()->default_value(0), "Choose problem to solve (0=laplace, 1=laplace with rhs=1, 2=p-laplace with p=2 which ist equal to normal laplace but will be computed differently, >2: p-laplace)") \
        ("dof,n", boost::program_options::value<int>()->default_value(3), "choose degrees of freedom in one direction") \
        ("paraviewoutput", boost::program_options::value<string>()->default_value("output"), "filename of paraview output file (only base name: output/<FILENAME>.<time>.csv") \
        ("dimension,d", boost::program_options::value<int>()->default_value(2), "dimension") \
        ("solver,s", boost::program_options::value<int>()->default_value(0), "choose solver: 0 = umfpack, 1 = CG, 2 = Jacobi-PCG, 3 = BlockJacobi-PCG") \
        ("blocksize,b", boost::program_options::value<int>()->default_value(3), "blockSize of BlockJacobi-Prec") \
        ;

    return desc;
}


/**

\author Lukas Duechting
\date 2018
\version 2.0

 */
int main(int argc, char* argv[])
{
    myLib::timerRestart();
    TIMER_LOCAL_INIT

    int testVar = 0;

    if(!myLib::directoryExists("output"))
    {
        myLib::createDirectory("output");
    }


    auto vm = myLib::parseCommandlineArguments(argc, argv, getProgramArguments);
    auto log_options = myLib::parseLoggerArguments(vm);
    myLib::initLogger(log_options);


    const int DIMENSION = vm["dimension"].as<int>();

    string outputFilename = vm["paraviewoutput"].as<string>();
    int order           = vm["order"].as<int>();
    int problem         = vm["problem"].as<int>();
    int n_x_direction   = vm["dof"].as<int>();
    int solver          = vm["solver"].as<int>();
    int blockSize       = vm["blocksize"].as<int>();

    int verbose         = 0;




    /**Lese mesh ein
     *
     */
    meshClass mesh;
    mesh.setDimension(DIMENSION);
    mesh.createTestMesh(n_x_direction);

    TIMER_MESSAGE("TIME TO READ/BUILD MESH: ", LOG(info))


    if (order == 1)
    {
        LOG(output) << "FE-Verfahren erster Ordnung aufgesetzt. ";
    }
    else if (order > 1 && order<=2)
    {
        mesh.addHighOrderPoints(order);
        LOG(output) << endl << "FE-Verfahren " << order << "ter Ordnung aufgesetzt. " << endl;

        TIMER_MESSAGE("TIME TO ADD HIGH ORDER POINTS: ", LOG(info))
    }
    else
    {
        LOG(fatal) << endl << "Diese Ordnung ist nicht verfügbar. " ;
        exit(EXIT_FAILURE);
    }


    mesh.print();


    LGS lgs(0, 1, 1, 0, 1);

    assemblyTemp assemblyT;


    int nodesandweightsK = 7;
    int nodesandweightsM = 7;
    int nodesandweightsB = 7;

    if (blockSize < 0)
    {
        blockSize = (int)(mesh.getnPoints()/4.0+0.9999);
    }


    if(problem == 0)
    {
        lgs.RHS_f = rhs_function_testgrid_sin;
        lgs.BC_dirichlet = dir0;

        assembly(nodesandweightsK, nodesandweightsM, nodesandweightsB, mesh, lgs);
    }
    else if(problem == 1)
    {
        lgs.RHS_f = rhs_f1;
        lgs.BC_dirichlet = dir0;

        assembly(nodesandweightsK, nodesandweightsM, nodesandweightsB, mesh, lgs);
    }
    else if(problem >= 2 && problem <= 100)
    {
        lgs.RHS_f = rhs_f1;
        lgs.BC_dirichlet = dir0;

        // Allocate lgs arrays
        allocateLGS(lgs, mesh);

        // Allocate temporary stuff and set shapeFunctions, nodesandweights etc
        assemblyT = assemblyTemp(nodesandweightsK, nodesandweightsM, nodesandweightsB, mesh);

        assemblyPLaplace(verbose, assemblyT, nodesandweightsK, nodesandweightsM, nodesandweightsB, mesh, lgs, problem);
    }
    else
    {
        LOG(fatal) << endl << "Choose a valid problem. ";
        exit(EXIT_FAILURE);
    }


    if (verbose)
    {
        lgs.K.print();
        PRINT_VECTOR(lgs.b)
    }

    TIMER_MESSAGE("TIME TO ASSEMBLY: ", LOG(info))


    if(problem < 2)
    {
        if(solver == 0)
        {
            // Loese mit Umfpack
            LOG(info) << endl << "USING UMFPACK TO SOLVE...";
            solve_FEM_umfpack(&lgs, &mesh);
        }
        else if(solver == 1 || (solver == 3 && blockSize == 0))
        {
            // Loese mit CG-Verfahren
            LOG(info) << endl << "USING CONJUGATE GRADIENT METHOD TO SOLVE..." ;
            conjugateGradient(verbose, lgs.K,lgs.b,lgs.u);
        }
        else if(solver == 2)
        {
            // Loese mit Jacobi-PCG-Verfahren
            LOG(info) << endl << "USING JACOBI-PCG-METHOD TO SOLVE..." ;
            PrecConjugateGradient(verbose, lgs.K, lgs.b, lgs.u, "jacobi");
        }
        else if(solver == 3)
        {
            // Loese mit Block-Jacobi-PCG-Verfahren
            LOG(info) << endl << "USING BLOCKJACOBI-PCG-METHOD (BLOCKSIZE = " << blockSize <<") TO SOLVE..." ;
            PrecConjugateGradient(verbose, lgs.K, lgs.b, lgs.u, "blockjacobi",blockSize);
        }
        else if(solver == 4)
        {
            // Loese mit Newton-Verfahren
            LOG(info) << endl << "USING NEWTONS-METHOD TO SOLVE..." ;

            std::vector<double> u0(lgs.b.size(),1.0);
            std::vector<double> F;
            F = lgs.K * u0 - lgs.b;
            newton NewtonsMethod(&u0);
            NewtonsMethod.setF(&F);
            NewtonsMethod.setJac(&lgs.K);
            //        NewtonsMethod.setPrecon("blockjacobi",100);

            // Fuehre eine Newton-Iteration durch
            NewtonsMethod.newtonIterate(verbose,lgs.u);

            // ________________ KANN FUER P_LAPLACE VERWENDET WERDEN__________________________
            //        vector<double> u0(lgs.b.size(),1.0);
            //        vector<double> F;
            //        vector<double> error;
            //        double maxNorm = 1.0;
            //        F = lgs.K * u0 - lgs.b;
            //        newton NewtonsMethod(&u0);
            //        NewtonsMethod.setF(&F);
            //        NewtonsMethod.setJac(&lgs.K);
            //
            //        while (maxNorm > EPS){
            //
            //            LOG(output) << "MAX_NORM(F): \t" <<
            //            NewtonsMethod.newtonIterate(verbose,lgs.u);
            //
            //            F = lgs.K * lgs.u - lgs.b;
            //            NewtonsMethod.setF()
            //            maxNorm = max_norm(&F);
            //        }
        }
    }

    else
    {
        // Loese mit Newton-Verfahren
        LOG(output) << "USING NEWTONS-METHOD TO SOLVE P-LAPLACE..." << endl;

        newton NewtonsMethod(&lgs.u);
        NewtonsMethod.setF(&lgs.b);
        NewtonsMethod.setJac(&lgs.K);

        double maxNorm = max_norm(lgs.b);



        NewtonsMethod.setPrecon("blockjacobi",blockSize);

        const double EPS = 1.0E-4;

        while (maxNorm > EPS){

            LOG(output) << "newton norm: " << maxNorm << endl;

            // Fuehre eine Newton-Iteration durch
            NewtonsMethod.newtonIterate(verbose,lgs.u);

            assemblyPLaplace(verbose, assemblyT, nodesandweightsK, nodesandweightsM, nodesandweightsB, mesh, lgs, 2);

            maxNorm = max_norm(lgs.b);
        }


    }


    LOG(output) ;
    LOG(output) << "FINISHED SOLVING SYSTEM";
    TIMER_MESSAGE("TIME FOR SOLVING: ", LOG(info))

    std::vector<double> error = computeError(*mesh.getNodelist(), u_exact_testgrid_sin, lgs.u);

    std::vector<double> norms;
    norms.push_back(max_norm(error));
    norms.push_back(h1_semi(error, lgs.K));

    LOG(output) << "Max Norm: " << std::setprecision(20) << norms[0] << "   H1-Seminorm: " << std::setprecision(20) << norms[1];

    outputToParaview(outputFilename, mesh, lgs);
    outputError(outputFilename, norms);

    return EXIT_SUCCESS;
}



