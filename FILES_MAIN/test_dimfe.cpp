
/**
 \file test_dimfe.cpp
 \brief Baue einige FE-Datenstrukturen auf, teste kleinere Funktionalitäten

 */

#include <stdlib.h>

#include "myLib_wrapper.hpp"
#include "meshClass.hpp"

#include "../FILES_SRC/lgs.hpp"
#include "../FILES_SRC/assembly.hpp"
#include "../FILES_SRC/shape_functions.hpp"
#include "../FILES_SRC/umfpack_solver.hpp"
#include "../FILES_SRC/conjugateGradient.hpp"


/**

\author Lukas Düchting
\date 2018
\version 3.0

 */
int main(int ac, char* av[])
{
    int verbose = false;
    int dimension = 2;
    int nNodesX = 5;

    auto vm = myLib::parseCommandlineArguments(ac, av);
    auto log_options = myLib::parseLoggerArguments(vm);
    myLib::initLogger(log_options);

    LOG(trace) << "Start program";
    
    meshClass mesh;
    mesh.setDimension(dimension);
    mesh.createTestMesh(nNodesX);

    mesh.addHighOrderPoints(2);

//    mesh.print() ;

//        meshClass refMesh;
//        refMesh.defineRefElementFirstOrder(dimension);
//        refMesh.print();
//    refMesh.addHighOrderPoints(2);
//    refMesh.determineTransformations();
//    refMesh.print();

    LGS lgs(0,1,1,0,1);

    int nodesandweightsK = 8;
    int nodesandweightsM = 8;
    int nodesandweightsB = 8;

    lgs.RHS_f = rhs_function_testgrid_sin;
    lgs.BC_dirichlet = dir0;

    assembly(nodesandweightsK, nodesandweightsM, nodesandweightsB, mesh, lgs);

//    mesh.print() ;

    // Loese das System
    int status;
    status = solve_FEM_umfpack(&lgs, &mesh);
//    conjugateGradient(verbose, lgs.K, lgs.b, lgs.u);

    std::cout << std::endl << std::endl << "SOLUTION: " << std::endl;
    PRINT_VECTOR(lgs.u)
    std::cout << std::endl << "max solution = " << *max_element(lgs.u.begin(), lgs.u.end());

    std::cout << std::endl << std::endl;

    return EXIT_SUCCESS;
}
