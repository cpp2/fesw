
/*! \file test_conjugateGradient.cpp
 \brief Test fuer die Konjugierte Gradienten Methode
 Fuehrt das CG-Verfahren sowie das PCG-Verfahren an einem einfachen Beispiel (vgl.
 https://de.wikipedia.org/wiki/CG-Verfahren ) durch.
 */

#include <stdio.h>
#include <vector>
#include <dirent.h>
#include <sys/stat.h>
#include <fstream>
#include <omp.h>
#include <random>
#include <cmath>

#include "matrix.hpp"
#include "denseMatrix.hpp"
#include "denseMatrix2.hpp"
#include "sparseMatrix.hpp"
#include "denseMatrix.hpp"

#include "../FILES_SRC/conjugateGradient.hpp"
#include "../FILES_SRC/newton.hpp"
#include "../FILES_SRC/specificFunctions.hpp"
#include "../FILES_SRC/assembly.hpp"
#include "../FILES_SRC/norm.hpp"



/** TESTFUNCTION
 * Testet Newton für Poissongleichung
\date 2018
\version 1.0

 */
int main(int argc, char **argv)
{
    auto vm = myLib::parseCommandlineArguments(argc, argv);
    auto log_options = myLib::parseLoggerArguments(vm);
    myLib::initLogger(log_options);

    LOG(trace) << "Start program";
    
    std::cout << std::endl << "\t \t Starte Test fuer Newton Verfahren" << std::endl ;
    int dimension       = 2 ;
    int order           = 1 ;
//    int problem         = 0 ;
    int n_x_direction   = 7 ;

    meshClass mesh ;
    mesh.setDimension(dimension) ;
    mesh.createTestMesh(n_x_direction) ;

    std::cout << "1" << std::endl;

    LGS lgs(0, 1, 1, 0, 1) ;

    lgs.RHS_f = rhs_function_testgrid_sin;
//    lgs.RHS_f = rhs_f1;
    lgs.BC_dirichlet = dir0;

    int nodesandweightsK = 7;
    int nodesandweightsM = 7;
    int nodesandweightsB = 7;

    std::cout << "2" << std::endl;

    assembly(nodesandweightsK, nodesandweightsM, nodesandweightsB, mesh, lgs) ;

    std::cout << "3" << std::endl;

    sparseMatrix K ;
    std::vector<double > b ;
    std::vector<double > F ;

    K = lgs.K ;
    b = lgs.b ;

    std::vector<double > sol ;
    //Erzeuge Einsvektor der Länge n
    std::vector<double > Ones ;

    Ones.resize(b.size(), 1.0) ;
    sol.resize(b.size(), 2.0) ;

    F = K * Ones - b ;

    std::cout << "4" << std::endl;

    newton Newton( &Ones ) ;
    Newton.setJac( &K ) ;
    Newton.setF( &F ) ;

    std::cout << "5" << std::endl;

//    string BJ = "BlockJacobi" ;
//
//    //Vorkonditionierer
//    Newton.setPrecon(BJ, 10) ;

//    Newton.newtonIterate(verbose, sol) ;

    std::vector<double > error ;
    error.resize(b.size(), 0.0 ) ;

    std::cout << "6" << std::endl;

//    conjugateGradient(verbose, lgs.K, lgs.b, lgs.u) ;

    error = sol - lgs.u ;

    double MaxErr = max_norm( error ) ;

//    F = K*sol - b ;

    std::cout << std::endl << "Der maximale Fehler zwischen der Newton Iteration und dem CGV betraegt:\t"<< MaxErr << std::endl ;

//    PRINT_VECTOR(F) ;

    return 0;
}

