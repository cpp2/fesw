
/**
 \file test_dimfe.cpp
 \brief Baue einige FE-Datenstrukturen auf, teste kleinere Funktionalitäten

 */

#include <stdlib.h>

#include "meshClass.hpp"
#include "../FILES_SRC/lgs.hpp"
#include "../FILES_SRC/assembly.hpp"
#include "../FILES_SRC/shape_functions.hpp"




/**
\author Lukas Düchting
\date 2018
\version 3.0
 */
int main(int argc, char **argv)
{
    auto vm = myLib::parseCommandlineArguments(argc, argv);
    auto log_options = myLib::parseLoggerArguments(vm);
    myLib::initLogger(log_options);

    LOG(trace) << "Start program";
    
    int verbose = 1;

    meshClass refMesh;
    refMesh.defineRefElementFirstOrder(3);
    //    refMesh.print();

    shape_functions Shape ;
    Shape.compute_shape_functions(0, refMesh) ;
    // Erzeuge die 4 Punkte, die mit den Knoten des Referenzelements korrespondieren
    point P0(0.0, 0.0, 0.0) ;
    point P1(1.0, 0.0, 0.0) ;
    point P2(0.0, 1.0, 0.0) ;
    point P3(0.0, 0.0, 1.0) ;
    // Vier doubles fuer die Auswertung
    double p0=0, p1=0, p2=0, p3=0 ;
    // Auswertung aller Vier Basisfunktionen in jedem der Punkte und anschliessender Print
    p0 = Shape.getPhi(&P0, 0) ;
    p1 = Shape.getPhi(&P0, 1) ;
    p2 = Shape.getPhi(&P0, 2) ;
    p3 = Shape.getPhi(&P0, 3) ;

    std::cout << "1. BF:\t" << p0 << " " << p1 << " " << p2 << " " << p3 << " " << std::endl ;

    p0 = Shape.getPhi(&P1, 0) ;
    p1 = Shape.getPhi(&P1, 1) ;
    p2 = Shape.getPhi(&P1, 2) ;
    p3 = Shape.getPhi(&P1, 3) ;

    std::cout << "2. BF:\t" << p0 << " " << p1 << " " << p2 << " " << p3 << " " << std::endl ;

    p0 = Shape.getPhi(&P2, 0) ;
    p1 = Shape.getPhi(&P2, 1) ;
    p2 = Shape.getPhi(&P2, 2) ;
    p3 = Shape.getPhi(&P2, 3) ;

    std::cout << "3. BF:\t" << p0 << " " << p1 << " " << p2 << " " << p3 << " " << std::endl ;

    p0 = Shape.getPhi(&P3, 0) ;
    p1 = Shape.getPhi(&P3, 1) ;
    p2 = Shape.getPhi(&P3, 2) ;
    p3 = Shape.getPhi(&P3, 3) ;

    std::cout << "4. BF:\t" << p0 << " " << p1 << " " << p2 << " " << p3 << " " << std::endl ;

}
