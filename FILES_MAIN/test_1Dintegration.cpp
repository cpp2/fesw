# include <cstdlib>
# include <iostream>
# include <iomanip>
# include <cmath>
# include <ctime>
# include <cstring>
#include  <math.h>

#include "myLib_wrapper.hpp"

#include "../FILES_SRC/1d_quadrule.hpp"
#include "../FILES_SRC/int_nodesandweights.hpp"
#include "../FILES_SRC/int_rule.hpp"

int main (int ac, char* av[]);
void test01 ( );
void test02 ( );
void test03 ( );
void test04 ( );
void test05 ( );
void test06 ( );
void test07 ( );
void test08 ( );
void test09 ( );
void test10 ( );

#define COUT LOG(info)

#define MACRO_r8vec_print r8vec_print


//****************************************************************************80

int main (int ac, char* av[])

//****************************************************************************80
//
//  Purpose:
//
//    MAIN is the main program for QWGW_PRB.
//
//  Discussion:
//
//    QWGW_PRB tests the QWGW library.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    22 February 2014
//
//  Author:
//
//    John Burkardt
//
{
    auto vm = myLib::parseCommandlineArguments(ac, av);
    auto log_options = myLib::parseLoggerArguments(vm);
    myLib::initLogger(log_options);

    LOG(output) << "QWGW_PRB:";
    LOG(output) << "  C++ version";
    LOG(output) << "  Test the QWGW library.";

    test01 ( );
    test02 ( );
    test03 ( );
    test04 ( );
    test05 ( );
    test06 ( );
    test07 ( );
    test08 ( );
    test09 ( );
    test10 ( );
    //
    //  Terminate.
    //
    LOG(output) << "\nQWGW_PRB:";
    LOG(output) << "  Normal end of execution.";

    return 0;
}
//****************************************************************************80

const double pi = 3.141592653589793;

void test01 ( )

//****************************************************************************80
//
//  Purpose:
//
//    TEST01 tests QWGW for the Chebyshev Type 1 weight.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    22 February 2014
//
//  Author:
//
//    John Burkardt
//
{
    double a;
    double *aj;
    double b;
    double *bj;
    int j;
    double mu0;
    int n;

    double *w;
    double *x;
    //
    //  Set the quadrature interval and number of points.
    //
    a = -1.0;
    b = +1.0;
    n = 5;

    COUT << "";
    COUT << "TEST01:";
    COUT << "  Compute points and weights for Gauss quadrature";
    COUT << "  with the Chebyshev Type 1 weight w(x) = 1/sqrt(1-x^2).";
    COUT << "  Order N = " << n << "";
    COUT << "  Interval = [" << a << "," << b << "]";
    //
    //  Set the recursion coefficients.
    //
    aj = new double[n];
    bj = new double[n];

    for ( j = 0; j < n; j++ )
    {
        aj[j] = 0.0;
    }

    bj[0] = 1.0 / 2.0;
    for ( j = 1; j < n - 1; j++ )
    {
        bj[j] = 1.0 / 4.0;
    }
    bj[n-1] = 0.0;

    for ( j = 0; j < n; j++ )
    {
        bj[j] = sqrt ( bj[j] );
    }

    mu0 = pi;
    //
    //  Compute the points and weights.
    //
    x = new double[n];
    w = new double[n];

    sgqf ( n, aj, bj, mu0, x, w );

    MACRO_r8vec_print ( n, x, "  Abscissas:" );
    MACRO_r8vec_print ( n, w, "  Weights:" );
    //
    //  Free memory.
    //
    delete [] aj;
    delete [] bj;
    delete [] w;
    delete [] x;

    return;
}
//****************************************************************************80

void test02 ( )

//****************************************************************************80
//
//  Purpose:
//
//    TEST02 tests QWGW for the Chebyshev Type 2 weight.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    22 February 2014
//
//  Author:
//
//    John Burkardt
//
{
    double a;
    double *aj;
    double b;
    double *bj;
    int j;
    double mu0;
    int n;
    double *w;
    double *x;
    //
    //  Set the quadrature interval and number of points.
    //
    a = -1.0;
    b = +1.0;
    n = 5;

    COUT << "";
    COUT << "TEST02:";
    COUT << "  Compute points and weights for Gauss quadrature";
    COUT << "  with the Chebyshev Type 2 weight w(x) = sqrt(1-x^2).";
    COUT << "  Order N = " << n << "";
    COUT << "  Interval = [" << a << "," << b << "]";
    //
    //  Set the recursion coefficients.
    //
    aj = new double[n];
    bj = new double[n];

    for ( j = 0; j < n; j++ )
    {
        aj[j] = 0.0;
    }

    for ( j = 0; j < n - 1; j++ )
    {
        bj[j] = 1.0 / 4.0;
    }
    bj[n-1] = 0.0;

    for ( j = 0; j < n; j++ )
    {
        bj[j] = sqrt ( bj[j] );
    }

    mu0 = pi / 2.0;
    //
    //  Compute the points and weights.
    //
    x = new double[n];
    w = new double[n];

    sgqf ( n, aj, bj, mu0, x, w );

    MACRO_r8vec_print ( n, x, "  Abscissas:" );
    MACRO_r8vec_print ( n, w, "  Weights:" );
    //
    //  Free memory.
    //
    delete [] aj;
    delete [] bj;
    delete [] w;
    delete [] x;

    return;
}
//****************************************************************************80

void test03 ( )

//****************************************************************************80
//
//  Purpose:
//
//    TEST03 tests QWGW for the Gegenbauer weight.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    22 February 2014
//
//  Author:
//
//    John Burkardt
//
{
    double a;
    double *aj;
    double alpha;
    double b;
    double *bj;
    int j;
    double jr;
    double mu0;
    int n;
    double *w;
    double *x;
    //
    //  Set the quadrature interval and number of points.
    //
    a = -1.0;
    b = +1.0;
    n = 5;
    //
    //  Set the weight function parameter.
    //
    alpha = 0.25;

    COUT << "";
    COUT << "TEST03:";
    COUT << "  Compute points and weights for Gauss quadrature";
    COUT << "  with the Gegenbauer weight w(x) = (1-x^2)^alpha.";
    COUT << "  Order N = " << n << "";
    COUT << "  ALPHA = " << alpha << "";
    COUT << "  Interval = [" << a << "," << b << "]";
    //
    //  Set the recursion coefficients.
    //
    aj = new double[n];
    bj = new double[n];

    for ( j = 0; j < n; j++ )
    {
        aj[j] = 0.0;
    }

    for ( j = 0; j < n - 1; j++ )
    {
        jr = ( double ) ( j + 1 );
        bj[j] = ( jr * ( 2.0 * alpha + jr ) )
                      / ( 4.0 * pow ( alpha + jr, 2 ) - 1.0 );
    }
    bj[n-1] = 0.0;

    for ( j = 0; j < n; j++ )
    {
        bj[j] = sqrt ( bj[j] );
    }

    mu0 = r8_gamma ( alpha + 1.0 ) * r8_gamma ( 0.5 ) / r8_gamma ( alpha + 1.5 );
    //
    //  Compute the points and weights.
    //
    x = new double[n];
    w = new double[n];

    sgqf ( n, aj, bj, mu0, x, w );

    MACRO_r8vec_print ( n, x, "  Abscissas:" );
    MACRO_r8vec_print ( n, w, "  Weights:" );
    //
    //  Free memory.
    //
    delete [] aj;
    delete [] bj;
    delete [] w;
    delete [] x;

    return;
}
//****************************************************************************80

void test04 ( )

//****************************************************************************80
//
//  Purpose:
//
//    TEST04 tests QWGW for the generalized Hermite weight.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    22 February 2014
//
//  Author:
//
//    John Burkardt
//
{
    double *aj;
    double alpha;
    double *bj;
    int j;
    double jr;
    double mu0;
    int n;
    double *w;
    double *x;
    //
    //  The quadrature interval is (-oo,+oo).
    //  Set the number of points.
    //
    n = 5;
    //
    //  Set the weight function parameter.
    //
    alpha = 2.0;

    COUT << "";
    COUT << "TEST04:";
    COUT << "  Compute points and weights for Gauss quadrature";
    COUT << "  with the generalized Hermite weight w(x) = |x|^alpha * exp(-x^2).";
    COUT << "  ALPHA = " << alpha << "";
    COUT << "  Order N = " << n << "";
    COUT << "  Interval = (-oo,+oo)";
    //
    //  Set the recursion coefficients.
    //
    aj = new double[n];
    bj = new double[n];

    for ( j = 0; j < n; j++ )
    {
        aj[j] = 0.0;
    }

    for ( j = 0; j < n - 1; j++ )
    {
        jr = ( double ) ( j + 1 );
        if ( ( j % 2 ) == 0 )
        {
            bj[j] = ( jr + alpha ) / 2.0;
        }
        else
        {
            bj[j] = jr / 2.0;
        }
    }
    bj[n-1] = 0.0;

    for ( j = 0; j < n; j++ )
    {
        bj[j] = sqrt ( bj[j] );
    }

    mu0 = r8_gamma ( ( alpha + 1.0 ) / 2.0 );
    //
    //  Compute the points and weights.
    //
    x = new double[n];
    w = new double[n];

    sgqf ( n, aj, bj, mu0, x, w );

    MACRO_r8vec_print ( n, x, "  Abscissas:" );
    MACRO_r8vec_print ( n, w, "  Weights:" );
    //
    //  Free memory.
    //
    delete [] aj;
    delete [] bj;
    delete [] w;
    delete [] x;

    return;
}
//****************************************************************************80

void test05 ( )

//****************************************************************************80
//
//  Purpose:
//
//    TEST05 tests QWGW for the generalized Laguerre weight.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    22 February 2014
//
//  Author:
//
//    John Burkardt
//
{
    double *aj;
    double alpha;
    double *bj;
    int j;
    double jr;
    double mu0;
    int n;
    double *w;
    double *x;
    //
    //  The quadrature interval is [0,+oo).
    //  Set the number of points.
    //
    n = 5;
    //
    //  Set the weight function parameter.
    //
    alpha = 2.0;

    COUT << "";
    COUT << "TEST05:";
    COUT << "  Compute points and weights for Gauss quadrature";
    COUT << "  with the generalized Laguerre weight w(x) = x^alpha * exp(-x).";
    COUT << "  Order N = " << n << "";
    COUT << "  ALPHA = " << alpha << "";
    COUT << "  Interval = [0,+oo)";
    //
    //  Set the recursion coefficients.
    //
    aj = new double[n];
    bj = new double[n];

    for ( j = 0; j < n; j++ )
    {
        jr = ( double ) ( j + 1 );
        aj[j] = alpha + 2.0 * jr - 1.0;
    }

    for ( j = 0; j < n - 1; j++ )
    {
        jr = ( double ) ( j + 1 );
        bj[j] = jr * ( alpha + jr );
    }
    bj[n-1] = 0.0;

    for ( j = 0; j < n; j++ )
    {
        bj[j] = sqrt ( bj[j] );
    }

    mu0 = r8_gamma ( alpha + 1.0 );
    //
    //  Compute the points and weights.
    //
    x = new double[n];
    w = new double[n];

    sgqf ( n, aj, bj, mu0, x, w );

    MACRO_r8vec_print ( n, x, "  Abscissas:" );
    MACRO_r8vec_print ( n, w, "  Weights:" );
    //
    //  Free memory.
    //
    delete [] aj;
    delete [] bj;
    delete [] w;
    delete [] x;

    return;
}
//****************************************************************************80

void test06 ( )

//****************************************************************************80
//
//  Purpose:
//
//    TEST06 tests QWGW for the Hermite weight.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    22 February 2014
//
//  Author:
//
//    John Burkardt
//
{
    double *aj;
    double *bj;
    int j;
    double jr;
    double mu0;
    int n;
    double *w;
    double *x;
    //
    //  The quadrature interval is (-oo,+oo).
    //  Set the number of points.
    //
    n = 5;

    COUT << "";
    COUT << "TEST06:";
    COUT << "  Compute points and weights for Gauss quadrature";
    COUT << "  with the Hermite weight w(x) = exp(-x^2).";
    COUT << "  Order N = " << n << "";
    COUT << "  Interval = (-oo,+oo)";
    //
    //  Set the recursion coefficients.
    //
    aj = new double[n];
    bj = new double[n];

    for ( j = 0; j < n; j++ )
    {
        aj[j] = 0.0;
    }

    for ( j = 0; j < n - 1; j++ )
    {
        jr = ( double ) ( j + 1 );
        bj[j] = jr / 2.0;
    }
    bj[n-1] = 0.0;

    for ( j = 0; j < n; j++ )
    {
        bj[j] = sqrt ( bj[j] );
    }

    mu0 = sqrt ( pi );
    //
    //  Compute the points and weights.
    //
    x = new double[n];
    w = new double[n];

    sgqf ( n, aj, bj, mu0, x, w );

    MACRO_r8vec_print ( n, x, "  Abscissas:" );
    MACRO_r8vec_print ( n, w, "  Weights:" );
    //
    //  Free memory.
    //
    delete [] aj;
    delete [] bj;
    delete [] w;
    delete [] x;

    return;
}
//****************************************************************************80

void test07 ( )

//****************************************************************************80
//
//  Purpose:
//
//    TEST07 tests QWGW for the Jacobi weight.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    22 February 2014
//
//  Author:
//
//    John Burkardt
//
{
    double a;
    double *aj;
    double alpha;
    double b;
    double beta;
    double *bj;
    int j;
    double jr;
    double mu0;
    int n;
    double *w;
    double *x;
    //
    //  Set the quadrature interval and number of points.
    //
    a = -1.0;
    b = +1.0;
    n = 5;
    //
    //  Set the weight function parameters.
    //
    alpha = 0.25;
    beta = 0.75;

    COUT << "";
    COUT << "TEST07:";
    COUT << "  Compute points and weights for Gauss quadrature";
    COUT << "  with the Jacobi weight w(x) = (1-x^2)^alpha*(1+x)^beta";
    COUT << "  Order N = " << n << "";
    COUT << "  ALPHA = " << alpha << "";
    COUT << "  BETA =  " << beta << "";
    COUT << "  Interval = [" << a << "," << b << "]";
    //
    //  Set the recursion coefficients.
    //
    aj = new double[n];
    bj = new double[n];

    for ( j = 0; j < n; j++ )
    {
        jr = ( double ) ( j + 1 );
        aj[j] = ( beta - alpha ) * ( beta + alpha )
                      / ( alpha + beta + 2.0 * jr - 2.0 )
                      / ( alpha + beta + 2.0 * jr );
    }

    for ( j = 0; j < n - 1; j++ )
    {
        jr = ( double ) ( j + 1 );
        bj[j] = 4.0 * jr * ( alpha + jr ) * ( beta + jr )
                      * ( alpha + beta + jr )
                      / ( pow ( alpha + beta + 2.0 * jr, 2 ) - 1.0 )
                      / pow ( alpha + beta + 2.0 * jr, 2 );
    }
    bj[n-1] = 0.0;

    for ( j = 0; j < n; j++ )
    {
        bj[j] = sqrt ( bj[j] );
    }

    mu0 = pow ( 2.0, alpha + beta + 1.0 )
                    * r8_gamma ( alpha + 1.0 ) * r8_gamma ( beta + 1.0 )
                    / r8_gamma ( alpha + beta + 2.0 );
    //
    //  Compute the points and weights.
    //
    x = new double[n];
    w = new double[n];

    sgqf ( n, aj, bj, mu0, x, w );

    MACRO_r8vec_print ( n, x, "  Abscissas:" );
    MACRO_r8vec_print ( n, w, "  Weights:" );
    //
    //  Free memory.
    //
    delete [] aj;
    delete [] bj;
    delete [] w;
    delete [] x;

    return;
}
//****************************************************************************80

void test08 ( )

//****************************************************************************80
//
//  Purpose:
//
//    TEST08 tests QWGW for the Laguerre weight.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    22 February 2014
//
//  Author:
//
//    John Burkardt
//
{
    double *aj;
    double *bj;
    int j;
    double jr;
    double mu0;
    int n;
    double *w;
    double *x;
    //
    //  The quadrature interval is [a,+oo).
    //  Set the number of points.
    //
    n = 5;

    COUT << "";
    COUT << "TEST08:";
    COUT << "  Compute points and weights for Gauss quadrature";
    COUT << "  with the Laguerre weight w(x) = exp(-x).";
    COUT << "  Order N = " << n << "";
    COUT << "  Interval = [0,+oo)";
    //
    //  Set the recursion coefficients.
    //
    aj = new double[n];
    bj = new double[n];

    for ( j = 0; j < n; j++ )
    {
        jr = ( double ) ( j + 1 );
        aj[j] = 2.0 * jr - 1.0;
    }

    for ( j = 0; j < n - 1; j++ )
    {
        jr = ( double ) ( j + 1 );
        bj[j] = jr * jr;
    }
    bj[n-1] = 0.0;

    for ( j = 0; j < n; j++ )
    {
        bj[j] = sqrt ( bj[j] );
    }

    mu0 = 1.0;
    //
    //  Compute the points and weights.
    //
    x = new double[n];
    w = new double[n];

    sgqf ( n, aj, bj, mu0, x, w );

    MACRO_r8vec_print ( n, x, "  Abscissas:" );
    MACRO_r8vec_print ( n, w, "  Weights:" );
    //
    //  Free memory.
    //
    delete [] aj;
    delete [] bj;
    delete [] w;
    delete [] x;

    return;
}
//****************************************************************************80

void test09 ( )

//****************************************************************************80
//
//  Purpose:
//
//    TEST09 tests QWGW for the Legendre weight.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    22 February 2014
//
//  Author:
//
//    John Burkardt
//
{
    double a;
    double *aj;
    double b;
    double *bj;
    int j;
    double jr;
    double mu0;
    int n;
    double *w;
    double *x;
    //
    //  Set the quadrature interval and number of points.
    //
    a = -1.0;
    b = +1.0;
    n = 5;

    COUT << "";
    COUT << "TEST09:";
    COUT << "  Compute points and weights for Gauss quadrature";
    COUT << "  with the Legendre weight w(x) = 1.";
    COUT << "  Order N = " << n << "";
    COUT << "  Interval = [" << a << "," << b << "]";
    //
    //  Set the recursion coefficients.
    //
    aj = new double[n];
    bj = new double[n];

    for ( j = 0; j < n; j++ )
    {
        aj[j] = 0.0;
    }

    for ( j = 0; j < n - 1; j++ )
    {
        jr = ( double ) ( j + 1 );
        bj[j] = jr * jr / ( 4.0 * jr * jr - 1.0 );
    }
    bj[n-1] = 0.0;

    for ( j = 0; j < n; j++ )
    {
        bj[j] = sqrt ( bj[j] );
    }

    mu0 = 2.0;
    //
    //  Compute the points and weights.
    //
    x = new double[n];
    w = new double[n];

    sgqf ( n, aj, bj, mu0, x, w );

    MACRO_r8vec_print ( n, x, "  Abscissas:" );
    MACRO_r8vec_print ( n, w, "  Weights:" );
    //
    //  Free memory.
    //
    delete [] aj;
    delete [] bj;
    delete [] w;
    delete [] x;

    return;
}


double polynomialEvaluation(double x, std::vector<double> coef)
{
    double evaluation = 0;

    for( int i = 0 ; i < coef.size(); i++ )
    {
        evaluation += coef[i] * pow(x, (double)i);
    }

    return evaluation;
}


double polynomialIntegration(double x, std::vector<double> coef)
{
    double evaluation = 0;

    for( int i = 0 ; i < coef.size(); i++ )
    {
        evaluation += coef[i] / (double) (i + 1) * pow(x, ((double)(i + 1)));
    }

    return evaluation;
}



void test10()
{
    int dimension = 1;
    int nRules = 10;

    std::vector<int_nodesandweights> intRules;
    for (int ruleIndex = 1; ruleIndex < nRules+1; ruleIndex++)
    {
        int_nodesandweights temp;
        temp.burkardtNodesAndWeightsWrapper(dimension, ruleIndex);
        intRules.push_back(temp);
    }

    COUT << std::endl << std::endl << "TEST 10";

    std::vector<double> coef;

    for (int monomialIndex = 1; monomialIndex <= nRules; monomialIndex++)
    {
        coef.push_back(1.0/(monomialIndex*100.0));

        std::stringstream s;
        s << "\np = ";
        for (int i = 0; i<coef.size(); i++)
        {
            s << " + " << coef[i] * 10 << " * x^" << i;
        }
        COUT << s.str() << std::endl;

        for (int ruleIndex = 0; ruleIndex < nRules; ruleIndex++)
        {
            //            intRules[ruleIndex].print();

            std::vector<double> evaluateAtX;
            evaluateAtX.resize(intRules[ruleIndex].getNIntpoints());
            for (int intPoint = 0; intPoint < evaluateAtX.size(); intPoint++)
            {
                evaluateAtX[intPoint] =polynomialEvaluation(intRules[ruleIndex].getXpoint(intPoint)->getX(), coef);
            }
            double integral = integral_ref(evaluateAtX, *intRules[ruleIndex].getW());
            COUT << "rule: " << ruleIndex << " \tIntegral: " << integral << " \texact integral: " << polynomialIntegration(1.0,coef) - polynomialIntegration(0.0,coef) << " \terror: " << abs(integral - polynomialIntegration(1.0,coef) + polynomialIntegration(0.0,coef));
        }
    }
}



