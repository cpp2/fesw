/*!
 * \file test_triangledunavantrule.cpp
 * \brief von John Burkardt zur Verfügung gestellter standard test für die dunavant Integrationsregeln auf Dreiecken
 */


# include <cstdlib>
# include <iostream>
# include <iomanip>
# include <cmath>
# include <cstring>

# include "myLib_wrapper.hpp"

# include "../FILES_SRC/triangle_dunavant_rule.hpp"


using std::cout;
using std::setw;

void test01 ( );
void test02 ( );
void test03 ( );
void test04 ( );
void test05 ( );



int verbose = 0;


//****************************************************************************80

int main(int argc, char **argv)
{
    auto vm = myLib::parseCommandlineArguments(argc, argv);
    auto log_options = myLib::parseLoggerArguments(vm);
    myLib::initLogger(log_options);

    LOG(trace) << "Start program";
//****************************************************************************80
//
//  Purpose:
//
//    MAIN is the main program for TRIANGLE_DUNAVANT_RULE_PRB.
//
//  Discussion:
//
//    TRIANGLE_DUNAVANT_RULE_PRB tests the TRIANGLE_DUNAVANT_RULE library.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    11 December 2006
//
//  Author:
//
//    John Burkardt
//


    timestamp_dun ( );
    cout << "\n";
    cout << "TRIANGLE_DUNAVANT_RULE_PRB:\n";
    cout << "  C++ version\n";
    cout << "  Test the TRIANGLE_DUNAVANT_RULE library.\n";

    test01 ( );
    test02 ( );
    test03 ( );
    test04 ( );
    test05 ( );
    //
    //  Terminate.
    //
    cout << "\n";
    cout << "TRIANGLE_DUNAVANT_RULE_PRB:\n";
    cout << "  Normal end of execution.\n";
    cout << "\n";
    timestamp_dun ( );

    return 0;
}
//****************************************************************************80

void test01 ( )

//****************************************************************************80
//
//  Purpose:
//
//    TEST01 tests DUNAVANT_RULE_NUM, DUNAVANT_DEGREE, DUNAVANT_ORDER_NUM.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    11 December 2006
//
//  Author:
//
//    John Burkardt
//
{
    int degree;
    int order_num;
    int rule;
    int rule_num;

    if(verbose)cout << "\n";
    if(verbose)cout << "TEST01\n";
    if(verbose)cout << "  DUNAVANT_RULE_NUM returns the number of rules;\n";
    if(verbose)cout << "  DUNAVANT_DEGREE returns the degree of a rule;\n";
    if(verbose)cout << "  DUNAVANT_ORDER_NUM returns the order of a rule.\n";

    rule_num = dunavant_rule_num ( );

    if(verbose)cout << "\n";
    if(verbose)cout << "  Number of available rules = " << rule_num << "\n";
    if(verbose)cout << "\n";
    if(verbose)cout << "      Rule    Degree     Order\n";
    if(verbose)cout << "\n";

    for ( rule = 1; rule <= rule_num; rule++ )
    {
        order_num = dunavant_order_num ( rule );
        degree = dunavant_degree ( rule );
        if(verbose)cout << "  " << setw(8) << rule
            << "  " << setw(8) << degree
            << "  " << setw(8) << order_num << "\n";
    }

    return;
}
//****************************************************************************80

void test02 ( )

//****************************************************************************80
//
//  Purpose:
//
//    TEST02 tests DUNAVANT_RULE.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    11 December 2006
//
//  Author:
//
//    John Burkardt
//
{
    int order;
    int order_num;
    int rule;
    int rule_num;
    double *wtab;
    double wtab_sum;
    double *xytab;

    if(verbose)cout << "\n";
    if(verbose)cout << "TEST02\n";
    if(verbose)cout << "  DUNAVANT_RULE returns the points and weights\n";
    if(verbose)cout << "  of a Dunavant rule for the triangle.\n";
    if(verbose)cout << "\n";
    if(verbose)cout << "  In this test, we simply check that the weights\n";
    if(verbose)cout << "  sum to 1.\n";

    rule_num = dunavant_rule_num ( );

    if(verbose)cout << "\n";
    if(verbose)cout << "  Number of available rules = " << rule_num << "\n";

    if(verbose)cout << "\n";
    if(verbose)cout << "      Rule      Order     Sum of weights\n";
    if(verbose)cout << "\n";

    for ( rule = 1; rule <= rule_num; rule++ )
    {
        order_num = dunavant_order_num ( rule );

        xytab = new double[2*order_num];
        wtab = new double[order_num];

        dunavant_rule ( rule, order_num, xytab, wtab );

        wtab_sum = 0.0;
        for ( order = 0; order < order_num; order++ )
        {
            wtab_sum = wtab_sum + wtab[order];
        }

        if(verbose)cout << "  " << setw(8) << rule
            << "  " << setw(8) << order_num
            << "  " << setw(14) << wtab_sum << "\n";

        delete [] wtab;
        delete [] xytab;
    }
    return;
}
//****************************************************************************80

void test03 ( )

//****************************************************************************80
//
//  Purpose:
//
//    TEST03 tests DUNAVANT_RULE.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    12 December 2006
//
//  Author:
//
//    John Burkardt
//
{
    int rule;
    int rule_num;
    int suborder;
    int suborder_num;
    double *suborder_w;
    double *suborder_xyz;
    double xyz_sum;

    if(verbose)cout << "\n";
    if(verbose)cout << "TEST03\n";
    if(verbose)cout << "  DUNAVANT_RULE returns the points and weights\n";
    if(verbose)cout << "  of a Dunavant rule for the triangle.\n";
    if(verbose)cout << "\n";
    if(verbose)cout << "  In this test, we simply check that, for each\n";
    if(verbose)cout << "  quadrature point, the barycentric coordinates\n";
    if(verbose)cout << "  add up to 1.\n";

    rule_num = dunavant_rule_num ( );

    if(verbose)cout << "\n";
    if(verbose)cout << "      Rule    Suborder    Sum of coordinates\n";
    if(verbose)cout << "\n";

    for ( rule = 1; rule <= rule_num; rule++ )
    {
        suborder_num = dunavant_suborder_num ( rule );

        suborder_xyz = new double[3*suborder_num];
        suborder_w = new double[suborder_num];

        dunavant_subrule ( rule, suborder_num, suborder_xyz, suborder_w );

        if(verbose)cout << "\n";
        if(verbose)cout << "  " << setw(8) << rule
            << "  " << setw(8) << suborder_num << "\n";

        for ( suborder = 0; suborder < suborder_num; suborder++ )
        {
            xyz_sum = suborder_xyz[0+suborder*3]
                                   + suborder_xyz[1+suborder*3]
                                                  + suborder_xyz[2+suborder*3];
            if(verbose)cout << "                    "
                << "  " << std::setprecision(16) << setw(25) << xyz_sum << "\n";
        }

        delete [] suborder_w;
        delete [] suborder_xyz;
    }
    return;
}
//****************************************************************************80

void test04 ( )

//****************************************************************************80
//
//  Purpose:
//
//    TEST04 tests DUNAVANT_RULE.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    11 December 2006
//
//  Author:
//
//    John Burkardt
//
{
    int a;
    double area = 0.5;
    int b;
    double coef;
    double err;
    double exact;
    int i;
    int order;
    int order_num;
    double quad;
    int rule;
    int rule_max = 10;
    double value;
    double *wtab;
    double x;
    double *xytab;
    double y;

    if(verbose)cout << "\n";
    if(verbose)cout << "TEST04\n";
    if(verbose)cout << "  DUNAVANT_RULE returns the points and weights of\n";
    if(verbose)cout << "  a Dunavant rule for the unit triangle.\n";
    if(verbose)cout << "\n";
    if(verbose)cout << "  This routine uses those rules to estimate the\n";
    if(verbose)cout << "  integral of monomomials in the unit triangle.\n";

    for ( a = 0; a <= 10; a++ )
    {
        for ( b = 0; b <= 10 - a; b++ )
        {
            //
            //  Multiplying X**A * Y**B by COEF will give us an integrand
            //  whose integral is exactly 1.  This makes the error calculations easy.
            //
            coef = ( double ) ( a + b + 2 ) * ( double ) ( a + b + 1 );
            for ( i = 1; i <= b; i++ )
            {
                coef = coef * ( double ) ( a + i ) / ( double ) ( i );
            }

            if(verbose)cout << "\n";
            if(verbose)cout << "  Integrate " << coef
                << " * X^" << a
                << " * Y^" << b << "\n";
            if(verbose)cout << "\n";
            if(verbose)cout << "      Rule       QUAD           ERROR\n";
            if(verbose)cout << "\n";

            for ( rule = 1; rule <= rule_max; rule++ )
            {
                order_num = dunavant_order_num ( rule );

                xytab = new double[2*order_num];
                wtab = new double[order_num];

                dunavant_rule ( rule, order_num, xytab, wtab );

                quad = 0.0;

                for ( order = 0; order < order_num; order++ )
                {
                    x = xytab[0+order*2];
                    y = xytab[1+order*2];

                    if ( a == 0 && b == 0 )
                    {
                        value = coef;
                    }
                    else if ( a == 0 && b != 0 )
                    {
                        value = coef * pow ( y, b );
                    }
                    else if ( a != 0 && b == 0 )
                    {
                        value = coef * pow ( x, a );
                    }
                    else if ( a != 0 && b != 0 )
                    {
                        value = coef * pow ( x, a ) * pow ( y, b );
                    }
                    quad = quad + wtab[order] * value;
                }
                quad = area * quad;

                exact = 1.0;
                err = fabs ( exact - quad );

                if(verbose)cout << "  " << setw(8)  << rule
                    << "  " << setw(14) << quad
                    << "  " << setw(14) << err << "\n";

                delete [] wtab;
                delete [] xytab;
            }
        }
    }
    return;
}
//****************************************************************************80

void test05 ( )

//****************************************************************************80
//
//  Purpose:
//
//    TEST05 demonstrates REFERENCE_TO_PHYSICAL_T3.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    12 December 2006
//
//  Author:
//
//    John Burkardt
//
{
# define NODE_NUM 3

    double area;
    double area2;
    int i;
    int node;
    double node_xy[2*NODE_NUM] = {
        0.0, 0.0,
        1.0, 0.0,
        0.0, 1.0 };
    double node_xy2[2*NODE_NUM] = {
        1.0, 2.0,
        1.0, 1.0,
        3.0, 2.0 };
    int order;
    int order_num;
    int point_show = 2;
    int rule;
    double *w;
    double *xy;
    double *xy2;

    if(verbose)cout << "\n";
    if(verbose)cout << "TEST05\n";
    if(verbose)cout << "  REFERENCE_TO_PHYSICAL_T3 transforms a rule\n";
    if(verbose)cout << "  on the unit (reference) triangle to a rule on \n";
    if(verbose)cout << "  an arbitrary (physical) triangle.\n";

    rule = 2;

    order_num = dunavant_order_num ( rule );

    xy = new double[2*order_num];
    xy2 = new double[2*order_num];
    w = new double[order_num];

    dunavant_rule ( rule, order_num, xy, w );
    //
    //  Here is the reference triangle, and its rule.
    //
    if(verbose)cout << "\n";
    if(verbose)cout << "  The reference triangle:\n";
    if(verbose)cout << "\n";

    for ( node = 0; node < NODE_NUM; node++ )
    {
        if(verbose)cout << "  " << setw(8)  << node+1
            << "  " << setw(14) << node_xy[0+node*2]
                                           << "  " << setw(14) << node_xy[1+node*2] << "\n";
    }

    area = triangle_area ( node_xy );

    if(verbose)cout << "\n";
    if(verbose)cout << "  Rule " << rule << " for reference triangle\n";
    if(verbose)cout << "  with area = " << area << "\n";
    if(verbose)cout << "\n";
    if(verbose)cout << "                X               Y               W\n";
    if(verbose)cout << "\n";

    for ( order = 0; order < order_num; order++ )
    {
        if(verbose)cout << "  " << setw(8)  << order
            << "  " << setw(14) << xy[0+order*2]
                                      << "  " << setw(14) << xy[1+order*2]
                                                                << "  " << setw(14) << w[order] << "\n";
    }
    //
    //  Transform the rule.
    //
    reference_to_physical_t3 ( node_xy2, order_num, xy, xy2 );
    //
    //  Here is the physical triangle, and its transformed rule.
    //
    if(verbose)cout << "\n";
    if(verbose)cout << "  The physical triangle:\n";
    if(verbose)cout << "\n";

    for ( node = 0; node < NODE_NUM; node++ )
    {
        if(verbose)cout << "  " << setw(8)  << node+1
            << "  " << setw(14) << node_xy2[0+node*2]
                                            << "  " << setw(14) << node_xy2[1+node*2] << "\n";
    }

    area2 = triangle_area ( node_xy2 );

    if(verbose)cout << "\n";
    if(verbose)cout << "  Rule " << rule << " for physical triangle\n";
    if(verbose)cout << "  with area = " << area2 << "\n";
    if(verbose)cout << "\n";
    if(verbose)cout << "                X               Y               W\n";
    if(verbose)cout << "\n";

    for ( order = 0; order < order_num; order++ )
    {
        if(verbose)cout << "  " << setw(8)  << order
            << "  " << setw(14) << xy2[0+order*2]
                                       << "  " << setw(14) << xy2[1+order*2]
                                                                  << "  " << setw(14) << w[order] << "\n";
    }

    delete [] w;
    delete [] xy;
    delete [] xy2;

    return;
# undef NODE_NUM
}
