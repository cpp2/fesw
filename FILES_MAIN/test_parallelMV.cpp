/*!
 * \file test_parallelMV.cpp
 * \brief Führt eine parallele Matrixmultiplikation aus, dense und sparse
 */

#include <math_macros.hpp>
#include <iostream>
#include <iomanip>
#include <string>
#include <map>
#include <random>
#include <cmath>
#include <omp.h>

// my math lib
#include "sparseMatrix.hpp"
#include "denseMatrix.hpp"
#include "math_toolbox.hpp"

#include "myLib_wrapper.hpp"


int main (int argc, char *argv[])
{
    std::random_device r;
    std::default_random_engine e1(r());
    std::uniform_real_distribution<double> uniform_dist(1, 6);

    std::default_random_engine e2(r());
    std::uniform_int_distribution<int> uniform_dist_int(0, 3);

    auto vm = myLib::parseCommandlineArguments(argc, argv);
    auto log_options = myLib::parseLoggerArguments(vm);
    myLib::initLogger(log_options);

    TIMER_LOCAL_INIT

    int n = 1000;

    denseMatrix m(n, n);
    std::vector<double> v;
    v.resize(n);

    for (int i = 0; i<n; i++){
        v[i] = uniform_dist(e1);
        for (int j = 0; j<n; j++){
            if (uniform_dist_int(e2)==0)
                m.setEntry(i,j,uniform_dist(e1));
        }
    }

    if (n<20)
        m.print();

    sparseMatrix sparse;

    sparse.fillFromDense(&m);

    std::vector<double> res;
    res.resize(n);


    TIMER_MESSAGE("vor dense mult: ", LOG(info))

    matrixVectorMult(m, v, res, n);

    TIMER_MESSAGE("nach dense mult, vor sparse mult: ", LOG(info))

    matrixVectorMult(sparse, v, res, n);

    TIMER_MESSAGE("nach sparse mult: ", LOG(info))

}

